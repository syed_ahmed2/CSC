#include <cstdlib>    
#include <cmath>   
#include <fstream>    
#include <iomanip>     
#include <iostream>
#include <string>
using namespace std; 
#include "Question.h"

void Question::setText(string txt) {
    text = txt;
}
string Question::getText() const {
    return text;
}
Question::Question() {
    text = "";
}
Question::Question(string txt) {
    text = txt;
}
void Question::display() const {
    cout << text << endl;
}