#include <cstdlib>    
#include <cmath>   
#include <fstream>    
#include <iomanip>     
#include <iostream>
#include <string>
using namespace std; 
#include "SAquestion.h"

SAquestion::SAquestion() : Question() {
    correctAnswer = "";
}
SAquestion::SAquestion(string txt, string ans) : Question(txt) {
    correctAnswer = ans;
}

void SAquestion::setCorrectAnswer(string ans) {
    correctAnswer = ans;
}

string SAquestion::getCorrectAnswer() const {
    return correctAnswer;
}

bool SAquestion::isCorrect(string ans) const {
    return (ans == correctAnswer);
}