#include <cstdlib>    
#include <cmath>   
#include <fstream>    
#include <iomanip>     
#include <iostream>
#include <string>
#include "Quiz.h"
using namespace std;

int main() {
    Quiz myQuiz;
    myQuiz.question01.setText("what does WSU stand for?");
    myQuiz.question01.choices[0] = "Wayne State University";
    myQuiz.question01.choices[1] = "West South Up";
    myQuiz.question01.choices[2] = "Way Super Umpire";
    myQuiz.question01.choices[3] = "Wacky Socks Under";
    myQuiz.question01.setCorrectAnswer(1);

    myQuiz.question02.setText("what is the name of the creator of this program?");
    myQuiz.question02.setCorrectAnswer("syed");

    myQuiz.question03.setText("what does SC stand for?");
    myQuiz.question03.choices[0] = "Student Center";
    myQuiz.question03.choices[1] = "SnapChat";
    myQuiz.question03.choices[2] = "SuperCross";
    myQuiz.question03.choices[3] = "SuperCatch";
    myQuiz.question03.setCorrectAnswer(1);

    myQuiz.question04.setText("what is the professor's name?");
    myQuiz.question04.setCorrectAnswer("alberto");

    myQuiz.startAttempt();
    cout << (myQuiz.getScore() / 4) * 100 << endl;

}