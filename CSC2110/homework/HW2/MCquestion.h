#include <cstdlib>    
#include <cmath>   
#include <fstream>    
#include <iomanip>     
#include <iostream>
#include <string>
using namespace std;
#include "Question.h"

#ifndef MCQUESTION_H
#define MCQUESTION_H

class MCquestion : public Question {
private:
    int correctAnswer;
public:
    string choices[4];
    void setCorrectAnswer(int);
    int getCorrectAnswer() const;
    bool isCorrect(int) const;
    void display() const;
    MCquestion();
    MCquestion(string, int);
};
#endif 