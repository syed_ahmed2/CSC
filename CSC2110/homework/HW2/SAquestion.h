#include <cstdlib>    
#include <cmath>   
#include <fstream>    
#include <iomanip>     
#include <iostream>
#include <string>
using namespace std;
#include "Question.h"

#ifndef SAQUESTION_H
#define SAQUESTION_H

class SAquestion : public Question {
private:
    string correctAnswer;
public:
    void setCorrectAnswer(string);
    string getCorrectAnswer() const;
    bool isCorrect(string) const;
    SAquestion();
    SAquestion(string, string);
};
#endif 