#include <cstdlib>    
#include <cmath>   
#include <fstream>    
#include <iomanip>     
#include <iostream>
#include <string>
using namespace std;
#include "MCquestion.h"

MCquestion::MCquestion() : Question() {
    correctAnswer = 0;
}
MCquestion::MCquestion(string txt, int ans) : Question(txt) {
    correctAnswer = ans;
}

void MCquestion::setCorrectAnswer(int ans) {
    if (ans >= 0 && ans <= 3) {
        correctAnswer = ans;
    }
}

int MCquestion::getCorrectAnswer() const {
    return correctAnswer;
}

bool MCquestion::isCorrect(int ans) const {
    if (ans == correctAnswer) {
        return true;
    }
    else {
        return false;
    }
}
void MCquestion::display() const {
    Question::display();
    for (int i = 0; i <= 3; i++) {
        cout << i + 1 << ". " << choices[i] << endl;
    }
}