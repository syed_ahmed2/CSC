#include <cstdlib>    
#include <cmath>   
#include <fstream>    
#include <iomanip>     
#include <iostream>
#include <string>
#include "Quiz.h"
#include "MCquestion.h"
#include "SAquestion.h"
#include <iostream>
using namespace std;

Quiz::Quiz() {
    score = 0;
    question01 = MCquestion();
    question02 = SAquestion();
    question03 = MCquestion();
    question04 = SAquestion();

}
double Quiz::getScore() const {
    return score;
}
void Quiz::startAttempt() {
    int q1;
    string q2;
    int q3;
    string q4;
    question01.display();
    cout << "Enter choice: ";
    cin >> q1;
    if (question01.isCorrect(q1)) {
        cout << "right" << endl;
        score++;
    }
    else {
        cout << "wrong" << endl;
    }
    question02.display();
    cout << "Enter choice: ";
    cin >> q2;
    if (question02.isCorrect(q2)) {
        cout << "right" << endl;
        score++;
    }
    else {
        cout << "wrong" << endl;
    }

    question03.display();
    cout << "Enter choice: ";
    cin >> q3;
    if (question03.isCorrect(q3)) {
        cout << "right" << endl;
        score++;
    }
    else {
        cout << "wrong" << endl;
    }

    question04.display();
    cout << "Enter choice: ";
    cin >> q4;

    if (question04.isCorrect(q4)) {
        cout << "right" << endl;
        score++;
    }
    else {
        cout << "wrong" << endl;
    }
}