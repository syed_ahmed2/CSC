#include <cstdlib>    
#include <cmath>   
#include <fstream>    
#include <iomanip>     
#include <iostream>
#include <string>
using namespace std;

#ifndef QUESTION_H
#define QUESTION_H

class Question {
private:
    string text;
public:
    Question();
    Question(string);
    void setText(string);
    string getText() const;
    void display() const;
};
#endif 