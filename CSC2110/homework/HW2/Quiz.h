#include <cstdlib>    
#include <cmath>   
#include <fstream>    
#include <iomanip>     
#include <iostream>
#include <string>
using namespace std;
#include "MCquestion.h"
#include "SAquestion.h"

#ifndef QUIZ_H
#define QUIZ_H
class Quiz {
private:
    double score;
public:
    MCquestion question01;
    SAquestion question02;
    MCquestion question03;
    SAquestion question04;
    double getScore() const;
    void startAttempt();
    Quiz();
};
#endif 