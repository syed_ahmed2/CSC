#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;
#include "MCquestion.h"

void MCquestion::setText(string s) {
    text = s;
}

string MCquestion::getText() const {
    return text;
}

void MCquestion::setCorrectAnswer(int ans) {
    if (ans < 0 && ans > 3) {
        cout << endl;
    } else {
        correctAnswer = ans;
    }
}

int MCquestion::getCorrectAnswer() const {
    return correctAnswer;
}

bool MCquestion::isCorrect(int ans) const {
    if (ans - 1 == correctAnswer) {
        return true;
    }
    else {
        return false;
    }
}

void MCquestion::display() const {
    cout << text << endl;
    for (int i = 0; i <= 3; i++) {
        cout << endl;
        cout << i + 1 << ". " << choices[i];
    }
}

MCquestion::MCquestion() {
    text = "";
    correctAnswer = 0;
}

MCquestion::MCquestion(string s) {
    text = s;
}