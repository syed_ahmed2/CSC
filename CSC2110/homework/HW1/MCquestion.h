#ifndef MCQUESTION_H
#define MCQUESTION_H
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;


class MCquestion {
private:
    string text;
    int correctAnswer;
public:
    string choices[4];
    void setText(string);
    string getText() const;
    void setCorrectAnswer(int);
    int getCorrectAnswer() const;
    bool isCorrect(int) const;
    void display() const;
    MCquestion();
    MCquestion(string);
};
#endif
