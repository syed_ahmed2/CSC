#include "MCquestion.h"
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;

int main()
{
    MCquestion mc("In Pirates of the Caribben, what was Captain Jack Sparrow's ship name?");
    mc.choices[0] = "The Marauder";
    mc.choices[1] = "The Black Pearl";
    mc.choices[2] = "The Black Python";
    mc.choices[3] = "The Slytherin";
    mc.display();
    mc.setCorrectAnswer(1);
    int correctOption;
    cout << endl;
    cout << "Enter the correct choice: ";
    cin >> correctOption;
    if (mc.isCorrect(correctOption)) {
        cout << "correct" << endl;
    }
    else {
        cout << "incorrect" << endl;
    }
}