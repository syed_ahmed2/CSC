#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include "Question.h"
using namespace std;


void Question::setText(string t)
{
	text = t;
}
string Question::getText() const
{
	return text;
}
void Question::setPoints(int p)
{
	points = p;
}
int Question::getPoints() const
{
	return points;
}
void Question::display() const
{
	cout << "\n" << text << endl;
}
Question::Question()
{
	text = " ";
}
Question::Question(string t)
{
	text = t;
}