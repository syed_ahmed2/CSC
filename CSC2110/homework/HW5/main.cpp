#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include "NAquestion.h"
#include "Quiz.h"
using namespace std;
int main()
{
    Quiz q;
    NAquestion* ptrNAq;

    ptrNAq = new NAquestion("1", 2006, 2008, 6); 
    q.setQuestion(0, ptrNAq);

    ptrNAq = new NAquestion("2", 5, 7, 15);
    q.setQuestion(1, ptrNAq);

    ptrNAq = new NAquestion("3?", 2010, 2012, 3);
    q.setQuestion(2, ptrNAq);

    ptrNAq = new NAquestion("4?", 107, 109, 10);
    q.setQuestion(3, ptrNAq);

    ptrNAq = new NAquestion("5", 1929, 1931, 5);
    q.setQuestion(4, ptrNAq);

    q.startAttempt();

    NAquestion* temp = q.largestQuestion();
    temp->display();
    cout << "largest Question" << endl;
}