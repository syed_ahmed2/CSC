#pragma once
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
using namespace std;

class Question
{
private:
	string text;
	int points;
public:
	void setText(string t);
	string getText() const;
	void setPoints(int p);
	int getPoints() const;
	void display() const;
	Question();
	Question(string);
};