#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include "Question.h"
#include "NAquestion.h"
using namespace std;

void NAquestion::setCorrectAnswer(float correctMin, float correctMax)
{
	correctAnswerMax = correctMax;
	correctAnswerMin = correctMin;
}
float NAquestion::getCorrectAnswerMin() const
{
	return correctAnswerMin;
}
float NAquestion::getCorrectAnswerMax() const
{
	return correctAnswerMax;
}
int NAquestion::isCorrect(float correctValue) const
{
	return (correctValue > correctAnswerMin && correctValue < correctAnswerMax);
}
NAquestion::NAquestion()
{
	correctAnswerMin = 0;
	correctAnswerMax = 0;
}
NAquestion::NAquestion(string txt, float correctMin, float correctMax, int pts) :Question(txt)
{
	correctAnswerMin = correctMin;
	correctAnswerMax = correctMax;
	Question::setPoints(pts);
}
bool NAquestion::operator>=(const NAquestion& q)
{
	return getPoints() >= q.getPoints();
}