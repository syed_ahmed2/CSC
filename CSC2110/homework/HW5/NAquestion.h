#pragma once
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include "Question.h"
using namespace std;


class NAquestion :public Question
{
private:
	float correctAnswerMin;
	float correctAnswerMax;
public:
	void setCorrectAnswer(float, float);
	float getCorrectAnswerMin() const;
	float getCorrectAnswerMax() const;
	int isCorrect(float) const;
	NAquestion();
	NAquestion(string, float, float, int);
	bool operator>=(const NAquestion& q);
};