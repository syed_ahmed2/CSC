#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include "Quiz.h"
using namespace std;

double Quiz::getScore() const
{
	return score;
}
void Quiz::setQuestion(int position, NAquestion* na)
{

    if (position >= 0 && position <= 4)
    {
        if (questions[position] == NULL) { questions[position] = na; }
        else
        {
            delete questions[position];
            questions[position] = na;
        }
    }
}
NAquestion* Quiz::getQuestion(int position) const
{
    if (position < 0 || position > 4)
        return NULL;
    else
        return questions[position];
}
void Quiz::resetQuestions()
{
    for (int i = 0; i < 5; i++) { delete questions[i]; }
}
void Quiz::startAttempt()
{
    int rightCount = 0;
    int numberQuestions = 0;
    for (int i = 0; i < 5; i++)
    {
        if (questions[i] != NULL)
        {
            questions[i]->display();
            cout << "\nEnter Answer: ";
            numberQuestions++;
            float userAnswer;
            cin >> userAnswer;
            if (questions[i]->isCorrect(userAnswer)) { rightCount++; }
        }

    }
    cout << fixed << setprecision(2);
    cout << "\nFinal Score = " << ((double)(rightCount) / (double)(numberQuestions)) * 100 << endl;
    score = (double)(rightCount) / (double)(numberQuestions);
}

NAquestion* Quiz::largest(int i, int j)
{

    if (questions[i] == NULL)
        return NULL;

    if (i == j)
        return questions[i];

    NAquestion* temp = largest(i + 1, j);

    if (temp == NULL)
        return questions[i];

    if (*(questions[i]) >= (*temp))
        return questions[i];

    return temp;
}

NAquestion* Quiz::largestQuestion()
{
    return largest(0, 4);
}
Quiz::Quiz()
{
    for (int i = 0; i < 5; i++) { questions[i] = NULL; }
}
