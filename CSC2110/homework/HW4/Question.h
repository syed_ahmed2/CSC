#ifndef QUESTION_H
#define QUESTION_H
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
#include <iostream>
using namespace std;

class Question
{
private:
	string text;
	int points = 1;
public:
	void setText(string t);
	string getText() const;
	void setPoints(int);
	int getPoints() const;
	void display() const;
	Question();
	Question(string);
};
#endif