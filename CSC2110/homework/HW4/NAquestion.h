#ifndef QUESTION_NA
#define QUESTION_NA
#include "Question.h"
#include<iostream>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;

class NAquestion : public Question
{
private:
    float correctAnswerMin;
    float correctAnswerMax;

public:
    NAquestion();
    NAquestion(string text, float min, float max);
    void setCorrectAnswer(float min, float max);
    float getCorrectAnswerMin() const;
    float getCorrectAnswerMax() const;
    int isCorrect(float m);
    int operator+(const int value);
    friend ostream& operator<<(ostream& out, const NAquestion& oQuestion);
};
#endif