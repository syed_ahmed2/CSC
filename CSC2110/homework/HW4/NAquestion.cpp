#include "NAquestion.h"
#include<iostream>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;

NAquestion::NAquestion() : Question()
{
    correctAnswerMax = 0;
    correctAnswerMin = 0;
}

NAquestion::NAquestion(string text, float min, float max) : Question(text)
{
    correctAnswerMax = 0;
    correctAnswerMin = 0;
    if (max >= min)
    {
        correctAnswerMax = max;
        correctAnswerMin = min;
    }
}

void NAquestion::setCorrectAnswer(float min, float max)
{
    if (max >= min)
    {
        correctAnswerMin = min;
        correctAnswerMax = max;
    }
}

float NAquestion::getCorrectAnswerMin() const { return correctAnswerMin; }

float NAquestion::getCorrectAnswerMax() const { return correctAnswerMax; }

int NAquestion::isCorrect(float m)
{
    int i = 0;
    if (m > getCorrectAnswerMin() && m < getCorrectAnswerMax())
        i = getPoints();
    return i;

}

int NAquestion::operator+(const int value) { return getPoints() + value; }

ostream& operator<<(ostream& out, NAquestion& oQuestion)
{
    out << endl;
    oQuestion.display();
    out << " correctAnswerMin : " << oQuestion.getCorrectAnswerMin() << " correctAnswerMax : " << oQuestion.getCorrectAnswerMax() << endl;
    return out;
}