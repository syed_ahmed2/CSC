#include "NAquestion.h"
#include "Quiz.h"
#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;

double Quiz::getScore() const { 
    return score; 
}

void Quiz::setQuestion(int p, NAquestion* na)
{
    if (p >= 0 && p <= 4)
    {
        if (questions[p] == NULL) { questions[p] = na; }
        else
        {
            delete questions[p];
            questions[p] = na;
        }
    }
}

NAquestion* Quiz::getQuestion(int p) const
{
    if (p < 0 || p > 4)
        return NULL;
    else
        return questions[p];
}

void Quiz::resetQuestion()
{
    for (int i = 0; i < 5; i++) { 
        delete questions[i]; 
    }
}

void Quiz::startAttempt()
{
    int correct = 0;
    int numq = 0;
    for (int i = 0; i < 5; i++)
    {
        if (questions[i] != NULL)
        {
            questions[i]->display();
            cout << "\nEnter Answer: ";
            numq++;
            float userAnswer;
            cin >> userAnswer;
            if (questions[i]->isCorrect(userAnswer)) 
            { 
                cout << "correct" << endl;
                correct++; 
            }
            else
            {
                cout << "wrong" << endl;
            }
        }
        cout << endl;
        cout << "=======================" << endl;
    }

    cout << fixed << setprecision(2);
    cout << endl;
    cout << "Grade: "<< ((double)(correct) / (double)(numq))*100 << "%" << endl;
}

Quiz::Quiz()
{
    for (int i = 0; i < 5; i++) { 
        questions[i] = NULL; 
    }
}