#include <iostream>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
#include "NAquestion.h"
#include "Quiz.h"
using namespace std;

int main()
{
    Quiz quiz;
    NAquestion* pq1 = new NAquestion("Guess what number I'm thinking", 194, 196);
    NAquestion* pq2 = new NAquestion("Guess what number second time ", 12, 14);
    NAquestion* pq3 = new NAquestion("Guess what number third time", 8, 10);
    NAquestion* pq4 = new NAquestion("Guess what number fourth time", 117, 119);
    NAquestion* pq5 = new NAquestion("Guess what number fifth time", 199, 201);

    quiz.setQuestion(0, pq1);
    quiz.setQuestion(1, pq2);
    quiz.setQuestion(2, pq3);
    quiz.setQuestion(3, pq4);
    quiz.setQuestion(4, pq5);

    quiz.startAttempt();
    quiz.resetQuestion();
}