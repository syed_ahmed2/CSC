#ifndef QUIZ_H
#define QUIZ_H
#include <iostream>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
#include "NAquestion.h"
using namespace std;

class Quiz
{
private:
    double score;
    NAquestion* questions[5];
public:
    double getScore() const;
    void setQuestion(int, NAquestion*);
    NAquestion* getQuestion(int) const;
    void resetQuestion();
    void startAttempt();
    Quiz();
};
#endif