#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
#include "Question.h"
#include<iostream>
#include<string>
using namespace std;

void Question::setText(string t) { 
    text = t; 
}

string Question::getText() const { 
    return text; 
}

void Question::setPoints(int p) {
    if (p > 1)
        this->points = p;
} 

int Question::getPoints() const {
    return points;
} 

void Question::display() const
{
    cout << endl << text << endl;
}

Question::Question() { 
    text = ""; 
}

Question::Question(string t) { 
    text = t; 
}