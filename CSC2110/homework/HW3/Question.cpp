#include <iostream>
#include <string>
using namespace std;

class Question {
private:
    string text;

public:

    Question() {
        text = "";
    }

    Question(string text) {
        this->text = text;
    }

    const string getText() {
        return text;
    }

    void setText(string text) {
        this->text = text;
    }

    void display() {
        cout << text << '\n';
    }
};