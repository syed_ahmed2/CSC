#include "NAquestion.cpp"
#include <iostream>

using namespace std;

class Quiz {
private:
    double score;
    NAquestion* questions[4];

public:
    Quiz() {
        for (int i = 0; i < 4; i++) {
            questions[i] = NULL;
        }
    }

    const double getScore() {
        return score;
    }

    void setQuestion(int index, NAquestion* question) {
        if (index >= 0 && index <= 3) {
            if (this->questions[index] == NULL) {
                this->questions[index] = question;
            }
            else {
                delete(questions[index]);
                questions[index] = question;
            }
        }
    }

    const NAquestion* getQuestion(int index) {
        if (index < 0 || index > 3) return NULL;
        if (questions[index] != NULL) {
            return questions[index];
        }

        return NULL;
    }

    void resetQuestion() {
        delete(questions);
        for (int i = 0; i < 4; i++) {
            questions[i] = NULL;
        }
        this->score = 0;
    }

    void startAttempt() {

        bool allNull = true;

        for (int i = 0; i < 4; i++) {
            if (questions[i] != NULL) {
                allNull = false;
                break;
            }
        }

        if (allNull) return;

        int validQuestions = 0;
        int correctAnswers = 0;

        for (int i = 0; i < 4; i++) {
            if (questions[i] == NULL) continue;

            validQuestions++;

            float userInput;
            questions[i]->display();
            cout << "Please Enter your answer!\n";
            cin >> userInput;

            if (questions[i]->isCorrect(userInput)) {
                correctAnswers++;
                cout << "The answer is correct\n";
            }
            else {
                cout << "The Answer is wrong!\n";
            }
        }

        score = correctAnswers * 1.0 / validQuestions * 100;

        cout << "Final Score is: " << score << '\n';
    }

};