#include "Quiz.cpp"
#include <cstdlib>    
#include <cmath>   
#include <fstream>    
#include <iomanip>     
#include <iostream>
#include <string>
using namespace std;

int main() {
    NAquestion* first = new NAquestion("This is question 1", 2, 10);
    NAquestion* second = new NAquestion("This is question 2", 20, 100);
    NAquestion* third = new NAquestion("This is question 3", 1, 10);

    Quiz quiz;
    quiz.setQuestion(0, first);
    quiz.setQuestion(1, second);
    quiz.setQuestion(2, third);

    quiz.startAttempt();
    quiz.resetQuestion();
    quiz.startAttempt();
    cout << quiz.getScore() << '\n';
}