#ifndef QUESTION_H
#define QUESTION_H

#include <iostream>
using namespace std;
class Question {
private:
    string text;
public:
    Question();
    Question(string);
    void setText(string);
    string getText() const;
    void display() const;
};
#endif 