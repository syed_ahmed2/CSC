#include "Question.cpp"

class NAquestion : public Question {
private:
    float correctAnswerMin;
    float correctAnswerMax;

public:
    NAquestion() {

    }

    NAquestion(string text, float min, float max) : Question(text) {
        correctAnswerMin = min;
        correctAnswerMax = max;
    }

    void setCorrectAnswer(float min, float max) {
        if (min <= max) {
            correctAnswerMax = max;
            correctAnswerMin = min;
        }
    }

    const float getCorrectAnswerMin() {
        return correctAnswerMin;
    }

    const float getCorrectAnswerMax() {
        return correctAnswerMax;
    }

    const bool isCorrect(float value) {
        return (value >= correctAnswerMin) && (value <= correctAnswerMax);
    }
};