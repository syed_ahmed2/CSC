#ifndef QUIZ_H
#define QUIZ_H
#include <iostream>
using namespace std;
class Quiz {
private:
    double score;
public:
    double getScore() const;
    void startAttempt();
    // default Constructor
    Quiz();
};
#endif 