#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include "Question.h"
#include "NAquestion.h"
using namespace std;



void NAquestion::setCorrectAnswer(float min, float max) 
{
    if (min < max) 
    {
        correctAnswerMax = max;
        correctAnswerMin = min;
    }
}

float NAquestion::getCorrectAnswerMin() const 
{ 
    return correctAnswerMin; 
}
float NAquestion::getCorrectAnswerMax() const 
{ 
    return correctAnswerMax; 
}
int NAquestion::isCorrect(float value) const 
{
    if (value > correctAnswerMin && value < correctAnswerMax) 
    {
        return 1;
    }
    else {
        return 0;
    }
}
NAquestion::NAquestion() :Question() 
{
    correctAnswerMax = correctAnswerMin = 0;
}
NAquestion::NAquestion(string txt, float min, float max, int pts) : Question(txt) 
{
    correctAnswerMax = max;
    correctAnswerMin = min;
}
bool NAquestion::operator>=(const NAquestion& q) 
{
    return this->getPoints() >= q.getPoints();
}
bool NAquestion::operator>(const NAquestion& q) 
{
    return this->getPoints() > q.getPoints();
}
bool NAquestion::operator<(const NAquestion& q) 
{
    return this->getPoints() < q.getPoints();
}
