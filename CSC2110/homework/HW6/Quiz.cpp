#include "NAquestion.h"
#include "Quiz.h"
#include <iostream>
#include <iomanip>
using namespace std;


double Quiz::getScore() const 
{ 
    return score; 
}
void Quiz::setQuestion(int position, NAquestion* na)
{
    if (position >= 0 && position <= 4)
    {
        if (questions[position] == NULL) { questions[position] = na; }
        else
        {
            delete questions[position];
            questions[position] = na;
        }
    }
}

NAquestion* Quiz::getQuestion(int position) const
{
    if (position < 0 || position > 4)
        return NULL;
    else
        return questions[position];
}

void Quiz::resetQuestion()
{
    for (int i = 0; i < 5; i++) 
    { 
        delete questions[i]; 
    }
}

void Quiz::startAttempt()
{
    int rightCount = 0;
    int numberQuestions = 0;
    for (int i = 0; i < 5; i++)
    {
        if (questions[i] != NULL)
        {
            questions[i]->display();
            cout << "\nEnter Answer: ";
            numberQuestions++;
            float userAnswer;
            cin >> userAnswer;
            if (questions[i]->isCorrect(userAnswer)) { rightCount++; }
        }
	cout << endl;
        cout << "----" << endl;
    }

    cout << fixed << setprecision(2);
    cout << "\nFinal Score = " << (double)(rightCount) / (double)(numberQuestions) << endl;
}

void Quiz::sort()
{
    int firstOutofOrder, location = 0;
    NAquestion* temp = nullptr;
    for (firstOutofOrder = 1; firstOutofOrder < 5; firstOutofOrder++)
    {
        if (*questions[firstOutofOrder] < *questions[firstOutofOrder - 1])
        {
            temp = questions[firstOutofOrder];
            location = firstOutofOrder;
            do
            {
                questions[location] = questions[location - 1];
                location--;
            } 
            while (location > 0 && questions[location - 1] > temp);
        }
        questions[location] = temp;
    }

}
NAquestion* Quiz::largest(int x, int y)
{

    if (questions[x] == NULL)
        return NULL;

    if (x == y)
        return questions[x];

    NAquestion* temp = largest(x + 1, y);

    if (temp == NULL)
        return questions[x];

    if (*(questions[x]) >= (*temp))
        return questions[x];

    return temp;
}


NAquestion* Quiz::largestQuestion()
{
    return largest(0,4);
}
Quiz::Quiz()
{
    for (int i = 0; i < 5; i++) 
    { 
        questions[i] = NULL;
    }
    score = 0;
}
