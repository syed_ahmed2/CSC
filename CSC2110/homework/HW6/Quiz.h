#pragma once
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include "NAquestion.h"
using namespace std;


class Quiz
{
private:
    double score;
    NAquestion* questions[5];
    NAquestion* largest(int x, int y);
public:
    double getScore() const;
    void setQuestion(int, NAquestion*);
    NAquestion* getQuestion(int) const;
    void resetQuestion();
    void startAttempt();
    NAquestion* largestQuestion();
    void sort();
    Quiz();
};
