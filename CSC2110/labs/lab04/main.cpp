
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
#include "account.h"
using namespace std;


int main() {
   class Account Account1;
   class Account Account2;

    Account1.name = "joe";
    Account2.name = "row";
    Account1.balance = 500;
    Account2.balance = 600;
    
    Account1.printaccount();
    Account2.printaccount();

    Account1.deposit(500);
    Account1.withdraw(200);
    Account2.deposit(500);
    Account2.withdraw(200);

    return 0;

}