#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;

class Account
{
    public:
        string name;
        double balance;
        void printaccount();
        void deposit(double x);
        void withdraw(double x);
        ~Account();
        Account() {
            cout << "Welcome"<< endl;
        }
};
