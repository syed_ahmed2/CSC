#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
#include "account.h"
using namespace std;


void Account::printaccount() {
    cout << name << " : " << balance << endl;
}

void Account::deposit(double x) {
    balance = balance + x;
    cout << "New balance: " << name << " : " << balance << endl;
}

void Account::withdraw(double x) {
    balance = balance - x;
    cout << "New balance: " << name << " : " << balance <<endl;
}
Account::~Account() {
    cout << "object is being deleted" << endl;  
}