#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;

void sum(int* array, int L)
{ 
    int sum = 0;
    for (int i = 0; i < L; i++)
        sum = sum + *(array + i);
    cout << "Sum: " << sum << endl;
}

int main() {
	int x, n;
	cout << "number of inputs: ";
	cin >>n;
	int *arr = new int(n);
	cout << "Enter " << n << " items" << endl;
	for (x = 0; x < n; x++) {
		cin >> arr[x];
	}
    sum(arr, n);
}