#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
#include <stdlib.h>
#include <time.h>
#include<stdio.h>
#include<string.h>
using namespace std;



class Rectangle
{
public:
    Rectangle();
    Rectangle(double, double);
    double Area();
    double Perimeter();
    void print();
    double getlength();
    virtual double Volume() = 0;
    double getwidth();
private:
    double length;
    double width;
};



Rectangle::Rectangle()
{
   length = 0;
   width = 0;
}


 double Rectangle::getlength() {
   return length;
 }
 
 double Rectangle::getwidth() {
   return width;
 }

Rectangle::Rectangle(double l, double w)
{
   length = l;
   width = w;
}

double Rectangle::Area(){
    return length*width;
}

double Rectangle::Perimeter(){
    return 2*(length*width);
}

void Rectangle::print(){
    cout<<"Length: "<<length<<endl;
    cout<<"Width: "<<width<<endl;
    cout<<"Area: "<<Area()<<endl;
    cout<<"Perimeter: "<<Perimeter()<<endl;
}

class Box : public Rectangle
{
    public:
        Box();
        Box(double, double, double);
        double Volume();
        void print();
        double Area();
        //double getheight();
    private:
        double height;
};


//double Box::getheight() {
//    return height;
//}

Box::Box():Rectangle()
{
    height = 0;
}

Box::Box(double l, double w, double h):Rectangle(l, w)
{
    height = h;
}

double Box::Volume(){
    return getlength() * getwidth() * height;
}

double Box::Area(){
  return 2*(getlength()*getwidth()) + 2*(height*getlength()) + 2*(getlength()*getlength());
}

void Box::print(){
    cout<<"Length: "<<getlength()<<endl;
    cout<<"Width: "<<getlength()<<endl;
    cout<<"Height: "<<height<<endl;
    cout<<"Area: "<<Area()<<endl;
    cout<<"Volume: "<<Volume()<<endl;
}

int main(){
    double x, y, z;
    double k, l, f;
    cout << "enter length, width, height for box 1: " << endl;
    cin  >> x;
    cin  >> y;
    cin >> z; 
    cout << "enter length, width, height for box 2: " << endl;
    cin >> k;
    cin >> l;
    cin >> f;

    Box obj1(x,y,z);
    Box obj2(k,l,f);

    cout<<"Box 1 details:-"<<endl;
    obj1.print();
    cout<<endl;
    cout<<"Box 2 details:-"<<endl;
    obj2.print();
}