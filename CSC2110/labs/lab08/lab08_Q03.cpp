#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;


int main() {
    double *trip1cost = new double;
    double *trip2cost = new double;
    double *trip3cost = new double;
    double *max;

    *trip1cost = 100.00;
    *trip2cost = 350.00;
    trip3cost = trip2cost;
    trip2cost = trip1cost;
    trip1cost = new double;
    *trip1cost = 175;
    *trip3cost = 275;


    cout << (*trip1cost) << endl;
    cout << (*trip2cost) << endl;
    cout << (*trip3cost) << endl;
    cout << fixed << showpoint << setprecision(2);

    cout << "Trip total cost: $" << (*trip1cost + *trip2cost + *trip3cost) << endl;

    max = trip1cost;
    if(*max < *trip2cost)
        max = trip2cost;
    if(*max < *trip3cost)
        max = trip3cost;

    cout << "highest trip cost: $" << *max << endl;
}