#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;


int main() {
    double *length;
    double *width;

    cout << fixed << showpoint << setprecision(2);

    length = new double; 
    *length = 6.5;

    width = new double;
    *width = 3.0;

    cout << "area: " << (*length) * (*width) << ", ";
    cout << "Perimeter: " << 2 * (*length + *width) << endl;
}