#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
#include <stdlib.h>
#include <time.h>
#include<stdio.h>
#include<string.h>
using namespace std; 

class Complex { 
private: 
    int real, imag; 
public: 
    Complex(int r = 0, int i =0)  {real = r;   imag = i;} 
    Complex operator - (Complex const &obj) { 
         Complex res; 
         res.real = real - obj.real; 
         res.imag = imag - obj.imag; 
         return res; 
    } 
    Complex operator / (Complex const &obj)
    {
         Complex res;
         if(obj.real==0&&obj.imag==0)
          {
              cout<<"Can't do operation" << endl;
              return 0;
          } 
         else
          {
              res.real=(real*obj.real+imag*obj.imag)/(obj.real*obj.real+obj.imag*obj.imag);
              res.imag=(-1*real*obj.imag+imag*obj.real)/(obj.real*obj.real+obj.imag*obj.imag);
              return res;
          }

       }
    void print() { cout << real << " + i(" << imag <<")"<< endl; } 
}; 
  
int main() 
{ 
    Complex c1(10, 5), c2(2, 4); 
    Complex c3 = c1 - c2; 
    cout<<"Subtraction "<< endl;
    c3.print(); 
    Complex c4=c1/c2;
    cout<<"Division " << endl;
    c4.print();
} 