#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
using namespace std;

void make(double alpha[]) {
    for (int i = 0; i <= 50; i++) {
        if (i < 25)
            alpha[i] = i * i;
        else
            alpha[i] = i * 3;
    }
}

void print(const double alpha[]) {
    int index = 0;
    for (int j = 1; j < 6; j++) {
        for (int i = 1; i <= 10; i++) {
            if (index < 50)
                cout << setw(5) << alpha[index];
            else
                break;
            index = index + 1;
    }
        cout << endl;
    }
}

int main()
{
    double alpha[50];
    make(alpha);
    print(alpha);

}
