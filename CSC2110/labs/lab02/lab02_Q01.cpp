#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
using namespace std;


bool isVowel(char x) {
    switch (x) {
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
            return true;
        default:
            return false;
    }
}

int main()
{
    char y;
    
    cout << "enter a letter: ";
    cin >> y;
    
    cout << isVowel(y) << endl;
}

