#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;


class Account{
    public:
        string name;
        double balance;
        void printaccount();
        void deposit(double x);
        void withdraw(double x);
        Account() {
            cout << "Welcome"<< endl;
        }
};

void Account::printaccount() {
    cout << name << " : " << balance << endl;
}

void Account::deposit(double x) {
    balance = balance + x;
    cout << "New balance: " << name << " : " << balance << endl;
}

void Account::withdraw(double x) {
    balance = balance - x;
    cout << "New balance: " << name << " : " << balance <<endl;
}

int main() {
    Account Account1;
    Account Account2;

    Account1.name = "joe";
    Account2.name = "row";
    Account1.balance = 500;
    Account2.balance = 600;
    
    Account1.printaccount();
    Account2.printaccount();

    Account1.deposit(500);
    Account1.withdraw(200);
    Account2.deposit(500);
    Account2.withdraw(200);

    return 0;

}
