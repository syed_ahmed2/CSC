#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
#include <stdlib.h>
#include <time.h>
using namespace std;

struct Student
{
   string name;
   int id;
   int arr[3];
};

int main()
{
   Student student;
   Student* studentPointer = &student;
    
    cout<<"Please enter a name: ";
    cin>>studentPointer->name;
    cout<<"Please enter an id: ";
    cin>>studentPointer->id;
    cout<<"Please enter a mark: ";
    cin>>studentPointer->arr[0];
    cout<<"Please enter a mark: ";
    cin>>studentPointer->arr[1];
    cout<<"Please enter a mark: ";
    cin>>studentPointer->arr[2];
    
    cout<<"Student info:"<<endl;
    cout<<"Name:"<<studentPointer->name <<endl;
    cout<<"Id:"<<studentPointer->id <<endl;
    cout<<"Mark 0:"<<studentPointer->arr[0] <<endl;
    cout<<"Mark 1:"<<studentPointer->arr[1] <<endl;
    cout<<"Mark 2:"<<studentPointer->arr[2] <<endl;
}