#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
#include <stdlib.h>
#include <time.h>
using namespace std;
class Timer
{
public:
int hr;
int mins;
Timer(int h,int min)
{
h=h+min/60;
min=min%60;
hr=h;
mins=min;
}
};

int* randArray(int x) {
   int* rA = new int[x];  
   for (int i=0; i<x; i++) {
           rA[i]   = rand();    
   }
   return rA; 
}


int main()
{
Timer* time= new Timer(10,20);
cout<<"Time: "<<time->hr<<" hr : "<<time->mins<<" min"<<endl;
int* rA = randArray(100); 
for (int i=0; i<100; i++) {
   cout << rA[i] << endl; 
}
delete[] rA; 
rA = 0;
}