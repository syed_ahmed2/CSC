#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
#include <stdlib.h>
#include <time.h>
#include<stdio.h>
#include<string.h>
using namespace std; 

template <class T>
T sumofSequence(T sum, T x, int n)
{
    sum = 1 + x*((n*(n+1)/2));
    return sum;
}

int main() 
{
    cout <<"sum(0,2,2): " << sumofSequence(0,2,2) << endl;
}