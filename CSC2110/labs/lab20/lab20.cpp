#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>
#include <string>
using namespace std;

void insertionSort(vector<float>& list)
{
    int firstOutOfOrder, location;
    float temp;
    for (firstOutOfOrder = 1; firstOutOfOrder < list.size(); firstOutOfOrder++)
    {
        if (list[firstOutOfOrder] < list[firstOutOfOrder - 1])
        {
            temp = list[firstOutOfOrder];
            location = firstOutOfOrder;
            do
            {
                list[location] = list[location - 1];
                location--;
            } while (location > 0 && list[location - 1] > temp);
            
            list[location] = temp;
            
        }
    }
}

int binarySearch(const vector<float> list, float searchItem)
{
    int first = 0;
    int last = list.size() - 1;
    int mid;

    bool found = false;
    while (first <= last && !found)
    {
        mid = (first + last) / 2;
        if (list[mid] == searchItem)
        {
            found = true;
        }
        else if (list[mid] > searchItem)
        {
            last = mid - 1;
        }
        else
        {
            first = mid + 1;
        }
    }

    if (found)
    {
        cout << "element found at " << mid << endl;
        return mid;
    }

    else
    {
        cout << "not found";
        return -1;
    }
}

int main()
{
    float x;
    vector<float> array;
    for (;;)
    {
        cout << "Enter a positive number (or a negative to stop): ";
        cin >> x;
        if (x < 0)
        {
            break;
        }
        array.push_back(x);
    }

    insertionSort(array);
    cout << "List of Values: " << endl;

    for (int i = 0; i < array.size(); i++)
    {
        cout << array[i] << " ";
    }

    cout << endl;
    cout << "find value ";
    float userInput;
    cin >> userInput;
    binarySearch(array, userInput);
}