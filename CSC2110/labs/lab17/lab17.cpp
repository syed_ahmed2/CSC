#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;


class InvalidQuestionValueException{
private:
string message;
public:
string what();
};


class SAquestion {
private:
string text;
string correctAnswer;
int points;
public:
void setText(string text);
void setCorrectAnswer(string correctAnswer);
string const getCorrectAnswer();
const int getPoints();
bool const isCorrect(string answer);
SAquestion(string text, string correctAnswer, string points);
};


int main() {

}