#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;

void s(int *x,int *y) 
{ 
	int z=*x; 
	*x=*y; 
	*y=z; 
} 
int main() 
{ 
	int x = 10;
    int y = 3; 
    cout<<"x:"<<x<<" y:"<<y << endl; 
	s(&x,&y); 
	cout<<"x:"<<x<<" y:"<<y << endl; 
} 