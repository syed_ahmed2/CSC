#include <iostream>
using namespace std;

void fillUp(int data[], int size);
void showArray(int values[], int size);

int main() {
    int size = 0;

    cout << "how many values? ";
    cin >> size;


    int *arr = new int[size];

    fillUp(arr, size);

    cout <<"you entered " << size << " numbers" << endl;
    showArray(arr, size);

    delete[] arr;
    return 0;


}

void fillUp(int data[], int length) {
    cout << "Enter " << length << " numbers:\n";
    for (int i = 0; i < length; i++) {
        cin >> data[i];
    }
}

void showArray(int values[], int size) {
    for (int i = 0; i < size; i++) {
        cout << values[i] << endl;
    }
}