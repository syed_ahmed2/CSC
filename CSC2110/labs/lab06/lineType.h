#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;
#include "pointType.h"

class line {
private:
    point p1;
    point p2;
public:
    line(point pt1, point pt2) { p1 = pt1; p2 = pt2; }
    void setP1(double ptx, double pty);
    void setP2(double ptx, double pty);
    point getP1();
    point getP2();
    void getSlope();
};