#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
#include "lineType.h"
using namespace std;

void line::setP1(double ptx, double pty) {
    p1.setX(ptx);
    p1.setY(pty);
}
void line::setP2(double ptx, double pty) {
    p2.setX(ptx);
    p2.setY(pty);
}

point line::getP1() {
    return p1;
}
point line::getP2() {
    return p2;
}

void line::getSlope() {
    if ((p2.getX() - p1.getX()) == 0) {
        cout << "vertical" << endl;
    }
    else if ((p2.getY() - p1.getY()) == 0) {
        cout << "horizontal" << endl;
    }
    else {
        double slope = (p2.getY() - p1.getY()) / (p2.getX() - p1.getX());
        double yInt = (p1.getY() - (slope * p1.getX()));

        cout << "slope: " + to_string(slope) << endl;
        cout << "equation: y = " << to_string(slope) << "x + " << to_string(yInt) << endl;
    }
}