#include <cstdlib>      
#include <cmath>       
#include <fstream>      
#include <iomanip>      
#include <iostream>    
#include <string>   
#include <cstring>
#include "pointType.h"
#include "lineType.h"
using namespace std;  

int main() {
    double xinput;
    double yinput;

    point p1;
    point p2;

    cout << "Enter x1: ";
    cin >> xinput;
    p1.setX(xinput);

    cout << "Enter y1: ";
    cin >> yinput;
    p1.setY(yinput);


    cout << "Enter x2: ";
    cin >> xinput;
    p2.setX(xinput);

    cout << "Enter y2: ";
    cin >> yinput;
    p2.setY(yinput);

    line line(p1, p2);

    p1.distance(p2);

    line.getSlope();
}