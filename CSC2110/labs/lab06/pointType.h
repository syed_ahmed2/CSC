#pragma once

class point {
private:
    double x;
    double y;
public:
    point() { x = 0; y = 0; }
    void setX(double ptx);
    void setY(double pty);
    double getX();
    double getY();
    void distance(point pt2);
};