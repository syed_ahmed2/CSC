#include<iostream>
using namespace std;

class Student {
public:
    string firstname;
    string lastname;

    Student()
    {

    }
    Student(string f, string l)
    {
        this->firstname = f;
        this->lastname = l;
    }

    const string& getFirstname() const {
        return firstname;
    }

    void setFirstname(const string& firstname) {
        Student::firstname = firstname;
    }

    const string& getLastname() const {
        return lastname;
    }

    void setLastname(const string& lastname) {
        Student::lastname = lastname;
    }
};

class NodeType {
public:
    Student data;
    NodeType* next;

    NodeType()
    {
        this->next = NULL;
    }
};


class LinkedListType {

private:
    int Size;
    NodeType* first;
    NodeType* last;

public:

    LinkedListType()
    {
        Size = 0;
        first = NULL;
        last = NULL;
    }
    const bool isEmpty()
    {
        if (first == NULL && last == NULL)
        {
            return true;
        }
        return false;
    }

    const int getSize()
    {
        return Size;
    }

    const void display()
    {
        NodeType* current = first;
        cout << "--------------------" << endl;
        while (current != NULL)
        {
            cout << current->data.firstname << " " << current->data.lastname << endl;
            current = current->next;
        }
        cout << endl;
    }
    void addFirst(Student studentData)
    {
        NodeType* newNode = new NodeType();
        newNode->data = studentData;

        if (isEmpty())
        {
            first = newNode;
            last = newNode;
        }
        else
        {
            newNode->next = first;
            first = newNode;
        }
        Size++;
    }
    void addLast(Student studentData)
    {
        NodeType* newNode = new NodeType();
        newNode->data = studentData;

        if (first == NULL)
        {
            first = last = newNode;
            Size++;
        }
        else
        {
            last->next = newNode;
            last = newNode;
        }
        Size++;
    }


    Student getFirst() const
    {
        if (first == NULL)
            cout << "empty." << endl;
        else
            return first->data;
    }

    Student getLast() const
    {
        if (first == NULL)
            cout << "empty." << endl;
        else
            return last->data;
    }

    void deleteFirst()
    {
        if (first == NULL)
            cout << "Error:empty." << endl;
        else if (first == last)
        {
            delete first;
            first = last = NULL;
            Size--;
        }
        else
        {
            NodeType* temp = first;
            first = first->next;
            delete temp;
            Size--;
        }

    }
    void deleteLast()
    {
        if (first == NULL)
            cout << "Error:empty." << endl;

        else if (first == last)
        {
            delete first;
            first = last = NULL;
            Size--;
        }
        else
        {
            NodeType* previous = first;
            while (previous->next != last)
                previous = previous->next;
            delete last;
            last = previous;
            last->next = NULL;
            Size--;
        }
    }
};


class Queue
{
public:
    LinkedListType linkedlist;
    void insert(Student studentData) {
        linkedlist.addLast(studentData);
    }
    Student deleteFirst() {
        Student stu = linkedlist.getFirst();
        linkedlist.deleteFirst();
        return stu;
    }
    bool isEmpty() {
        return linkedlist.isEmpty();
    }
    void display() {
        linkedlist.display();
    }
};

int main() {
    Student x("x", "y");
    Student x1("z", "k");
    Student x2("j", "l");

    Queue q;
    q.insert(x);
    q.insert(x1);
    q.insert(x2);

    q.display();

    q.deleteFirst();
    q.display();

    q.deleteFirst();
    q.display();

    q.deleteFirst();
    q.display();
}