#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
using namespace std;

int sum(const int array[], const int size)
{
    int sum = 0;
    for (int i = 0; i < size; i++)
    {
        sum += array[i];
    }
    return sum;
}

int recursiveSum(const int array[], const int size, const int position)
{
    if (position == size)
    {
        return 0;
    }
    return array[position] + recursiveSum(array, size, position + 1);
}

int recursiveMax(const int array[], const int size, const int position)
{
    if (position == size)
    {
        return position;
    }
    return max(array[position], recursiveMax(array, size, position + 1));
}

int recursiveMin(const int array[], const int size, const int position)
{
    if (position == size)
    {
        return position;
    }
    return min(array[position], recursiveMin(array, size, position + 1));
}


int main()
{
    int array[] = { 1,2,3,4,5,6,7,8,9,10 };
    int size = sizeof(array) / sizeof(array[0]);

    cout << "sum: " << sum(array, size) << endl;
    cout << "recursive sum: " << recursiveSum(array, size, 0) << endl;
    cout << "max value: " << recursiveMax(array, size, 0) << endl;
    cout << "min value: " << recursiveMin(array, size, 0) << endl;
}