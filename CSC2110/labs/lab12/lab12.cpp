#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
#include <stdlib.h>
#include <time.h>
#include<stdio.h>
#include<string.h>
using namespace std;


class shape
{
public:
    double x = 0;
    double y = 0;

    virtual void draw() = 0;

    virtual void move(double x, double y) = 0;

    void display() {
        cout << "new coordinates" << endl;
        cout << x << endl;
        cout << y << endl;
    };
};


class triangle : public shape
{
private:
    string color;
public:

    void move(double x1, double y1) {

        x = x1;
        y = y1;
    }

    void draw() {
        cout << x << endl;
        cout << y << endl;
    }

    void setColor(string s) {
        color = s;
    }
    string getColor() {
        return color;
    }

};


class square : public shape
{
private:
    string color;
public:

    void move(double x1, double y1) {

        x = x1;
        y = y1;
    }

    void draw() {
        cout << x << endl;
        cout << y << endl;
    }

    void setColor(string s) {
        color = s;
    }
    string getColor() {
        return color;
    }

};


int main() {

    square s1;
    triangle s2;
    double x1;
    double y1;
    double x2;
    double y2;
    string c;
    string x;
    string d;
    string k;
    
    cout << "enter x: ";
    cin >> x1;
    cout << "enter y: ";
    cin >> y1;
    cout << "enter color: ";
    cin >> c;
    s1.setColor(c);
    x = s1.getColor();
    s1.move(x1, y1);
    s1.display();
    cout << x << endl;

    cout << "enter x: ";
    cin >> x2;
    cout << "enter y: ";
    cin >> y2;
    cout << "enter color: ";
    cin >> d;
    s2.setColor(d);
    k = s2.getColor();
    s2.move(x2, y2);
    s2.display();
    cout << k << endl;

}