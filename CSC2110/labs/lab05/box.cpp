#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;
#include "Box.h"

Box::Box() : Rectangle() {
	height = 0;
}

void Box::setHeight(double h) {
	height = h;
}

double Box::getHeight() {
	return height;
}

Box::Box(double l, double w, double h) : Rectangle(l, w) {

	height = h;
}

double Box::Volume() {

	return (getLength()*getWidth()*height);
}

void Box::print() {

	Rectangle::print(); 

	cout << "Height: " << height << endl;
}

double Box::area() {

	return (2*(getWidth()*height)+2*(getLength()*height)+2*(getLength()*getWidth()));
}