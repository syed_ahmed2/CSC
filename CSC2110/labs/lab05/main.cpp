#include "Rectangle.h"
#include "Box.h"
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;

int main() {
	Box box1(1, 2, 3);
	Box box2(4, 5, 6);

	cout << "Box 1\n";
	box1.print();
	cout << "Area: " << box1.area() << endl;
	cout << "Volume: " << box1.Volume() << endl;
	cout << endl;
	cout << "Box 2" << endl;
	box2.print();
	cout << "Area: " << box2.area() << endl;
	cout << "Volume: " << box2.Volume() << endl;
}