#ifndef RECTANGLE_H
#define RECTANGLE_H

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;

class Rectangle {

private:

    double length;
    double width;

public:

    Rectangle();
    Rectangle(double, double);
    double area();
    double perimeter();
    void print();
    void setLength(double l);
    double getLength();
    void setWidth(double w);
    double getWidth();
};

#endif
