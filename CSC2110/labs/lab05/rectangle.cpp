#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;
#include "Rectangle.h"

Rectangle::Rectangle() {
    length = 0.0;
    width = 0.0;
}

    Rectangle::Rectangle(double l, double w) {

    length = l;
    width = w;
}

double Rectangle::getLength() {
    return length;
}

void Rectangle::setLength(double l) {
    length = l;
}

double Rectangle::getWidth() {
    return width;
}

void Rectangle::setWidth(double w) {
    width = w;
}

double Rectangle::area() {

    return length * width;
}

double Rectangle::perimeter() {

    return 2 * (length + width);
}

void Rectangle::print() {

    cout << "length: " << length << endl
        << "width: " << width << endl;
}