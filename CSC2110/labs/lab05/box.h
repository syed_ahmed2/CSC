#ifndef BOX_H
#define BOX_H

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;
#include "Rectangle.h"

class Box : public Rectangle {

private:

    double height;

public:

    Box();
    Box(double, double, double);
    double Volume();
    void print();
    double area();
    void setHeight(double h);
    double getHeight();
};

#endif
