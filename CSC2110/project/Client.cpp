#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include "Client.h";
using namespace std;


Client::Client(string i, string fn, string ln) //set private value accessable
{
    id = stoi(i);
    firstName = fn;
    lastName = ln;
    checking = NULL;
    saving = NULL;
}

void Client::setID(int i) // point value
{
    id = i;
}

int Client::getID() //return id 
{
    return id;
}

void Client::setName(string fn, string ln) //point value
{
    firstName = fn;
    lastName = ln;
}

string Client::getFirstName() //return firstname
{
    return firstName;
}

string Client::getLastName() //return lastname
{
    return lastName;
}
