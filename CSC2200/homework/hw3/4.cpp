#include <iostream>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <string>
using namespace std;

int Sum(const vector<int> arr) {
int x = arr[0];
int y = arr[0];
for (int i = 0; i < arr.size(); i++) {
if (arr[i] > x)
x = arr[i];
else if (arr[i] > y && arr[i] < x)
y = arr[i];
}
return x + y;
}



int Mult(const vector<int> arr) {
int x = arr[0];
int y = arr[0];
for (int i = 0; i < arr.size(); i++) {
if (arr[i] > x) {
x = arr[i];
}
else if (arr[i] < y) { 
y = arr[i];
}
}
if (y * y > x * x)
return y * y;
else
return x * x;
}

int main()
{
	vector<int> arr = { 1,2,3,4,5,6,7,8,9,10};
    cout << Mult(arr) <<endl;
    cout << Sum(arr) <<endl;
}

