
// SYED AHMED

#include "Text.h"
#include <iomanip>
using namespace std;
Text::Text ( const char *charSeq )
{
	bufferSize = strlen(charSeq) + 1;
	buffer = new char[bufferSize];
	strcpy(buffer, charSeq);
}

Text::Text ( const Text &other )
{
	bufferSize = other.bufferSize;
	buffer = new char[bufferSize];
	strcpy(buffer, other.buffer);
}

void Text::operator = ( const Text &other )
{
	if (this != &other) {
		if (buffer != nullptr)
			delete buffer;
		bufferSize = other.bufferSize;
		buffer = new char[bufferSize];
		strcpy(buffer, other.buffer);
	}
}

Text::~Text ()
{
	delete buffer;
}

int Text::getLength () const
{
	return bufferSize - 1;
}

char Text::operator [] ( int n ) const
{
	return buffer[n];
}

void Text::clear ()
{
	if (buffer != nullptr) {
		delete buffer;
		bufferSize = 0;
		buffer = nullptr;
	}
}

void Text::showStructure () const
{
	for (int i = 0; i < bufferSize - 1; i++)
		cout << setw(3) << i;
	cout << endl;
	for (int i = 0; i < bufferSize - 1; i++)
		cout << setw(3) << buffer[i];
	cout << endl;
}

Text Text::toUpper( ) const
{
	Text temp = *this;
	for (int i = 0; i < temp.bufferSize - 1; i++)
		temp.buffer[i] = toupper(temp.buffer[i]);
	return temp;
}

Text Text::toLower( ) const
{
	Text temp = *this;
	for (int i = 0; i < temp.bufferSize - 1; i++)
		temp.buffer[i] = tolower(temp.buffer[i]);
	return temp;
}

bool Text::operator == ( const Text& other ) const
{
	if (strcmp(buffer, other.buffer) == 0)
		return true;
	else
		return false;
}

bool Text::operator <  ( const Text& other ) const
{
	if (strcmp(buffer, other.buffer) < 0)
		return true;
	else
		return false;
}

bool Text::operator >  ( const Text& other ) const
{
	if (strcmp(buffer, other.buffer) > 0)
		return true;
	else
		return false;
}
