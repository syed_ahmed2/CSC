//package homework.hw6;

import java.io.*;
import java.util.*;

public class Assignment06_Q08 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner SCAN = new Scanner(new File("numbers.txt"));


        int arr[] = new int[6];


        for(int i=0;i<20;i++){
        int NEXT = SCAN.nextInt();
        arr[NEXT]++;
        }


        PrintWriter OUT = new PrintWriter(new File("output.txt"));


        OUT.println("Ratings\tFrequency");


        for(int i=1;i<=5;i++){
        OUT.println(i+"\t\t"+arr[i]);
        }


        OUT.flush();
        OUT.close();
        }
}
