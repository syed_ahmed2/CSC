//package homework.hw6;

class Square {
    private double side;


    public Square(double S) {
    super();
    side = S;
    }


    public Square(String N, double h) {
    side = h;
    }
    public double getArea() {
        double S2 = side*side;
    return S2;
    }
    }
    class ComparableSquare extends Square implements Comparable<ComparableSquare>{
    public ComparableSquare (double X){
    super(X);
    }
    @Override
    public int compareTo(ComparableSquare s) {
    return Double.valueOf(getArea()).compareTo(s.getArea());
    }
    }
    
    
    public class Assignment06_Q05 {
    public static void main(String[] args) {
    
        ComparableSquare SQUARE1 = new ComparableSquare(54);
        ComparableSquare SQUARE2 = new ComparableSquare(34);
    if(SQUARE1.compareTo(SQUARE2)>0)
    System.out.println("SQUARE1 BIGGEST SQUARE "+SQUARE1.getArea());
    else
    System.out.println("SQUARE2 BIGGEST SQUARE "+SQUARE2.getArea());
    }
    }
