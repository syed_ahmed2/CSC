//package homework.hw6;
import ToolKit.*;

public class Assignment06_Q04 {

    public static void main(String[] args) {

        GeometricObject[] GS = new GeometricObject[5];
        GS[0] = new MyRectangle2D();
        GS[3] = new MyRectangle2D(0, 0, 15, 6);
        GS[2] = new Square();
        GS[4] = new Square(0,0,35);
        GS[1] = new Circle2D();

        for (int i = 0; i < GS.length; i++) {
            System.out.println("shape number is: " + (i + 1) + "and area is: " + GS[i].getArea());
            if (GS[i] instanceof Colorable) {
                System.out.println(((Colorable)GS[i]).howToColor());
            }
        }

    }

}

class Square extends GeometricObject implements Colorable {

    private double length;
    private double width;
    private double side;

    Square() {
        this(0,0,10);
    }

    Square(double x, double y, double side) {
        this.length = x;
        this.width = y;
        this.side = side;
    }

    @Override
    public double getArea() {
        return side * side;
    }

    @Override
    public double getPerimeter() {
        return side * 4;
    }

    @Override
    public String howToColor() {
        return "Color all four sides.";
    }

    public double getlength() {
        return length;
    }

    public void setlength(double x) {
        this.length = x;
    }

    public double getwidth() {
        return width;
    }

    public void setwidth(double y) {
        this.width = y;
    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }
}

interface Colorable {

    String howToColor();
}
