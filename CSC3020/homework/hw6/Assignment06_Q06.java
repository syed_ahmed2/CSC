//package homework.hw6;

import java.io.*;
import java.util.*;

class ExceptionA extends Exception{
    private static final long serialVersionUID = -3690287947029041174L;
    public ExceptionA(String message) {
          System.out.println("EXCEPTION A AND "+ message);   
    }
}

class ExceptionB extends ExceptionA {
    private static final long serialVersionUID = 7916205703655991524L;
    public ExceptionB(String message) {
          super(message);
          System.out.println("EXCEPTION B AND"+ message);
    }
}

public class Assignment06_Q06 {

      public static void main(String[] args){
            try
            {
            Scanner sc= new Scanner(System.in);
            System.out.println("CHOICE 1:ExceptionA, CHOICE 2:ExceptionB, CHOICE 3:Null Pointer Exception, CHOICE 4:-IO Exception");
            System.out.println("ENTER EXCEPTION");
            int choice=sc.nextInt();
            switch(choice)
            {
            case 1:
            {
                throw new ExceptionA("EA THROW");
            }
            case 2:
            {
                throw new ExceptionB("EB THROW");
            }
            case 3:
            {
                throw new NullPointerException("NULLPTR THROW");
            }
            case 4:
            {
                  throw new IOException("IO THROW");
            }
            default:
            {
                  System.out.println("ILLEGAL CHOICE");
            }
            }
      }
            catch(ExceptionB ex)
            {
                  System.out.println("EA EB CATCH");
            }
            catch(IOException ex)
            {
                  System.out.println("IO CATCH");
            }
            catch(NullPointerException ex)
            {
                  System.out.println("NULLPTR CATCH");
            }
            catch(Exception e)
            {
System.out.println("UNKNOWN EXCEPTION"+e.getMessage()) ;
            }
            finally {
                  System.out.println("END");
            }
      }
}
