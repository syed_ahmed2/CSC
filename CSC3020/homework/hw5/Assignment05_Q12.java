package homework.hw5;
import java.util.*;

public class Assignment05_Q12 {
	public static void main(String args[])
    {
        Scanner sc=new Scanner(System.in);
        
        System.out.print("Manufacturer: ");
        String mf=sc.nextLine();


        System.out.print(" Cylinders: ");
        int cy=sc.nextInt();
        System.out.print("load capacity: ");
        int lc=sc.nextInt();
        
        Truck T1=new Truck(mf,cy,lc);


        System.out.println();
        System.out.print("Manufacturer: ");
        sc.nextLine();


        mf=sc.nextLine();
        System.out.print("Cylinders: ");
        cy=sc.nextInt();
        System.out.print("load capacity: ");
        lc=sc.nextInt();
        
        Truck T2=new Truck(mf,cy,lc);
        

        if(T1.equals(T2))
        {
			System.out.println();
            System.out.println("equal");
        }
        else{
			System.out.println();
            System.out.println("not equal");
        }
    }
}

class Vehicle{
    private String MF;
    private int CY;
    
    Vehicle()
    {
        MF="";
        CY=0;
    }
    
    Vehicle(String MF,int CY)
    {
        this.MF=MF;
        this.CY=CY;
    }
    
    public String toString()
    {
        return "Manufacturer: "+MF+"\ncylinders: "+CY;
    }
    
    public boolean equals(Object obj)
    {
        if(obj==this)
        {
            return true;
        }
        
        if((obj instanceof Vehicle)==false)
        {
            return false;
        }
        
        Vehicle x=(Vehicle)obj;
        if(x.MF.equals(MF) && x.CY==CY)
        {
            return true;
        }
        return false;
    }
}

class Truck extends Vehicle{
    private double LC;
    
    Truck()
    {
        LC=0;
    }
    
    Truck(String manufacturer,int noOfCyclinder,double loadCapacity)
    {
        super(manufacturer,noOfCyclinder);
        this.LC=loadCapacity;
    }
    
    public String toString()
    {
        String S;
		S =super.toString();
        return S+"\nLoad Capacity: "+LC;
    }
    
    public boolean equals(Object obj)
    {
        if(super.equals(obj)==false)
        {
            return false;
        }
        
        if(obj==this)
        {
            return true;
        }        
        if((obj instanceof Truck)==false)
        {
            return false;
        }        
        Truck t=(Truck)obj;
        if(t.LC==LC)
        {
            return true;
        }
        return false;
    }
}
