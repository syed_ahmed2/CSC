package homework.hw4;
import java.math.BigInteger;
public class Assignment04_Q06 {    
	public static void main(String[] args) {
	BigInteger n = new BigInteger(String.valueOf(Long.MAX_VALUE));
	BigInteger zero = new BigInteger("0");
	BigInteger five = new BigInteger("5");
	BigInteger three = new BigInteger("3");

	int x = 0;
	int y = 0;

	while (x < 25) {
		n = n.add(new BigInteger("1"));
		if ( n.remainder(five).compareTo(zero) == 0 || n.remainder(three).compareTo(zero) == 0) {
			System.out.print(n + "\t");
			y++;
			x++;
			if (y % 5 == 0) {
				System.out.println();
			}
		}
	}
} 
}
