package homework.hw4;
public class Assignment04_Q08 {
	public static void main(String[] args) {
		MyStringBuilder2 x = new MyStringBuilder2("sdfsdfsdfsdfg");
		System.out.println( x.append(new MyStringBuilder2(" dfgesfhdgsdfhdfhgf")));
		System.out.println(x.append(3));
		System.out.println(x.length());
		System.out.println(x.charAt(3));
		System.out.println(x.toLowerCase());
		System.out.println(x.substring(1, 8));
		System.out.println(x.toString());
	}
}
class MyStringBuilder2 {
    private String s;

	public MyStringBuilder2(String s) {
		this.s = s;
	}

	public MyStringBuilder2 append(MyStringBuilder2 s) {
		String x  = this.s; 
		x += s;
		return new MyStringBuilder2(x);
	}

	public MyStringBuilder2 append(int arr) {
		String x  = this.s; 
		x += arr + "\t";
		return new MyStringBuilder2(x);
	}

	public int length() {
		return s.length();
	}

	public char charAt(int idx) {
		return s.charAt(idx);
	}

	public MyStringBuilder2 toLowerCase() {
		String x = "";
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) >= 'A' && s.charAt(i) <= 'Z') {
				x += (char)(s.charAt(i) + 32) + "";
			}
		}
		return new MyStringBuilder2(x);
	}

	public MyStringBuilder2 substring(int x, int y) {
		String z = "";
		for (int i = x; i < y; i ++) {
			z += s.charAt(i) + "";
		}
		return new MyStringBuilder2(z);
	}

	public String toString() {
		return s;
	}
}
