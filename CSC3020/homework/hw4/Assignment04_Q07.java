package homework.hw4;

public class Assignment04_Q07 {
    public static void main(String[] args) {
        MyString2 x = new MyString2("JDFHKLGDFL;DFHKLGFHKGFH");
        System.out.println(x.getArray());
        System.out.println(x.substring(6).getArray());
        System.out.println(x.toUpperCase().getArray());
        System.out.println(MyString2.valueOf(true).getArray());
    }
}

class MyString2 {
    private char[] arr;
    public MyString2(String s) {
        arr = s.toCharArray();
    }
    public String getArray() {
        
        String x;
		x = new String(arr);
        return x;
    }
    public MyString2 substring(int y) {
        char[] x; 
		x= new char[arr.length - y];
        for (int z = y; z < arr.length; z++) {
            x[z - y] = arr[z];
        }
        MyString2 k; 
		k = new MyString2(new String(x));
        return k;
    }
    public MyString2 toUpperCase() {
        char[] x; 
		x = new char[arr.length];
        for (int z = 0; z < arr.length; z++) {
            x[z] = Character.toUpperCase(arr[z]);
        }
        MyString2 k; 
		k = new MyString2(new String(x));
        return k;
    }
    public static MyString2 valueOf(boolean b) {
        MyString2 x;
        if (b) {
            x = new MyString2("true");
        }
        else {
            x = new MyString2("false");
        }
        return x;
    }
}
