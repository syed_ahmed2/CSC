package homework.hw2;

import java.text.DecimalFormat;
import java.util.Scanner;
public class Assignment02_Q22 {
       public static void main(String[] args) {
              Scanner in = new Scanner(System.in);
              String names[] = new String[5];
              int votes[] = new int[5];
              double total = 0;
              int winner = 0;
              DecimalFormat d = new DecimalFormat("0.00");
              for(int i = 0; i < 5; ++i) {
                     System.out.print("Enter candidate name: ");
                     names[i] = in.next();
                     System.out.print("Enter number of votes " + names[i] + " got: ");
                     votes[i] = in.nextInt();
                     total += votes[i];
                     if(i > 0) {
                           if(votes[i] > votes[i-1]) {
                                  winner = i;
                           }
                     }
              }
              System.out.println("Candidate \t Votes Received \t % of Total Votes");
              for(int i = 0; i < 5; ++i) {
                     System.out.println(names[i] + "\t \t \t" + votes[i] + "\t \t \t" + d.format(100 * votes[i]/total));
              }
              System.out.println("Total: " + (int) total);
              System.out.println("The winner of the Election is " + names[winner] + ".");
       }
    }