package homework.hw2;

public class Assignment02_Q21C {
	public static void main(String[] args) {
		int x = 6;
		System.out.println("Pattern C");
		for (int i = 1; i <= x; i++) {
			for (int j = x - i; j >= 1; j--) {
				System.out.print("  ");
			}
			for (int z = i; z >= 1; z--) {
				System.out.print(z + " ");
			}
			System.out.println();
		}
	}
}
