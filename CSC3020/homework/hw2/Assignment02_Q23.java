package homework.hw2;
import java.util.Scanner;

public class Assignment02_Q23 {
   public static double[][] temps = new double[12][2];
   public static void main(String[] args) {
       getData();
       System.out.println("Average high temperature: "+averageHigh());
       System.out.println("Average low temperature: "+averageLow());
       System.out.println("The month with the highest temperature: "+indexHighTemp());
       System.out.println("The month with the lowest temperature: "+indexLowTemp());
   }
   public static void getData() {
       Scanner in = new Scanner(System.in);
       for (int i = 0; i < 12; i++) {
           System.out.println("Enter highest temperature for month " + (i + 1));
           temps[i][0] = in.nextDouble();
           System.out.println("Enter lowest temperature for month " + (i + 1));
           temps[i][1] = in.nextDouble();
       }
   }
   public static double averageHigh() {
       double average = 0;
       for (int i = 0; i < 12; i++) {
           average += temps[i][0];
       }
       return average / 12;
   }
   public static double averageLow() {
       double average = 0;
       for (int i = 0; i < 12; i++) {
           average += temps[i][1];
       }
       return average / 12;
   }
   public static int indexHighTemp() {
       int idx = 0;
       double max = Double.MIN_VALUE;
       for (int i = 0; i < 12; i++)
           if (temps[i][0] > max)
               {
               idx = i;
               max =temps[i][0];
               }
       return idx+1;
   }
   public static int indexLowTemp() {
       int idx = 0;
       double minimum = Double.MAX_VALUE;
       for (int i = 0; i < 12; i++)
           if (temps[i][1] < minimum)
               {
               idx = i;
               minimum = temps[i][1];
               }
       return idx+1;
   }
}