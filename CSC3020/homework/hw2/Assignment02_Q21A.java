package homework.hw2;

public class Assignment02_Q21A {
	public static void main(String[] args) {
		System.out.println("Pattern A");
		for (int i = 1; i <= 6; i++) {
			for (int j = 1; j <= i ; j++) {
				System.out.print(j + " ");
			}
			System.out.println();
		}
	}
}
