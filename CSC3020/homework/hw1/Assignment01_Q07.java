package homework.hw1;

import java.util.Scanner;

public class Assignment01_Q07 {

    public static void main(String[] args) {
        int r =  (int)(1 + (Math.random() * 20));
        int h = (int)(1 + (Math.random() * 20));
        double v = Math.PI * r * r * h;
        double sa = 2 * Math.PI * r * h;
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the length of side 1: ");
        double s1 = input.nextDouble();
        System.out.print("Enter the length of side 2: ");
        double s2 = input.nextDouble();
        System.out.print("Enter the length of side 3: ");
        double s3 = input.nextDouble();

        double perimeter = (s1 + s2 + s3) * 0.5;
        double area = Math.sqrt(perimeter * (perimeter - s1) * (perimeter - s2) * (perimeter - s3));
        System.out.println("Area: " + area);
        System.out.println("Radius: " + r);
        System.out.println("Height: " + h);
        System.out.printf("Volume: %.2f", v);
        System.out.printf("\n");
        System.out.printf("Surface area: %.2f", sa);
        System.out.printf("\n");

    }
}