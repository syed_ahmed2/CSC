package homework.hw1;
import java.util.Scanner;

public class Assignment01_Q08 {    
    public static void main(String[] args) {
        int a = 0, e = 0, i = 0, u = 0, o = 0, other = 0;
        String t;
        Scanner input = new Scanner(System.in);
        System.out.print("enter string: ");
        t = input.nextLine();

        for(int x = 0; x< t.length(); x++) {
            char vowel = t.charAt(x);
            if(vowel=='a') {
            a++;
            }
            else if (vowel=='e') {
            e++;
            }
            else if(vowel=='i') {
            i++;
            }
            else if (vowel=='o') {
            o++;
            }
            else if (vowel=='u') {
            u++;
            }
            else {
            other++;
            }
        }
        System.out.println("a:" + a);
        System.out.println("e:" + e);
        System.out.println("i:" + i);
        System.out.println("o:" + o);
        System.out.println("u:" + u);
        System.out.println("other:" + other);
    }
}