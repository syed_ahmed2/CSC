package homework.hw3;

public class Assignment03_Q07 {
    public static void main(String[] args) {
        Queue queue = new Queue();
        for (int i = 0; i < 20; i++) {
            queue.enqueue(i + 1);
        }
        int queueSize = queue.getSize();
        for (int j = 0; j < queueSize; j++) {
            System.out.printf("Value %d is: %d%n", j+1, queue.dequeue());
        }
    }
}
class Queue {
    private int[] arr;
    private int s;
    public Queue() {
        arr = new int[8];
    }
    public boolean empty() {
        return s == 0;
    }
    public int getSize() {
        return s;
    }
    public void enqueue(int v) {
        if (s >= arr.length) {
            int[] temp = new int[arr.length * 2];
            System.arraycopy(arr, 0, temp, 0, arr.length);
            arr = temp;
        }
        arr[s] = v;
        s++;
    }
    public int dequeue() {
        int[] temp = new int[arr.length - 1];
        int returnval = arr[0];
        System.arraycopy(arr, 1, temp, 0, arr.length - 1);
        arr = temp;
        s--;
        return returnval;
    }
}