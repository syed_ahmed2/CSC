package homework.hw3;

public class RegularPolygon {
	private int n;
	private double side;
	private double x;
	private double y;
	RegularPolygon() {
		n = 3;
		side = 1;
		x = y = 0;
	}
	RegularPolygon(int newn, double newSide) {
		n = newn;
		side = newSide;
		x = y = 0;
	}
	RegularPolygon(int newn, double newSide, double newX, double newY) {
		n = newn;
		side = newSide;
		x = newX;
		y = newY;
	}
	public void setnvar(int newn) {
		n = newn;
	}
	public void setsidelen(double newSide) {
		side = newSide;
	}
	public void setxcord(double newX) {
		x = newX;
	}
	public void setycord(double newY) {
		y = newY;
	}
	public int getN() {
		return n;
	}
	public double getside() {
		return side;
	}
	public double getx() {
		return x;
	}
	public double gety() {
		return y;
	}
	public double getPerimeter() {
		return side * n;
	}
	public double getArea() {
		return (n * Math.pow(side, 2)) / (4 * Math.tan(Math.PI / n));
	}
}