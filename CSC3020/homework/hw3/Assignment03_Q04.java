package homework.hw3;

public class Assignment03_Q04 {
    public static void main(String[] args) {
		RegularPolygon RP1 = new RegularPolygon();
		RegularPolygon RP2 = new RegularPolygon(6, 4);
		RegularPolygon RP3 = new RegularPolygon(10, 4, 5.6, 7.8);
		System.out.println(" Regular Polygon Objects   Perimeter    Area  ");
		System.out.printf( "       Object# 1         %8.2f     %6.2f  ", RP1.getPerimeter(), RP1.getArea());
		System.out.println();
		System.out.printf( "       Object# 2         %8.2f     %6.2f  ", RP2.getPerimeter(), RP2.getArea());
		System.out.println();
		System.out.printf( "       Object# 3         %8.2f     %6.2f  ", RP3.getPerimeter(), RP3.getArea());
		System.out.println();
	}
}



