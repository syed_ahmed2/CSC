package homework.hw3;

public class Assignment03_Q06 {
    public static void main(String[] args) {
        MyInteger int1 = new MyInteger(23);
        MyInteger int2 = new MyInteger(56);
        MyInteger int3 = new MyInteger(2);
        MyInteger int4 = new MyInteger(23);
        System.out.printf("%d is prime? %s%n", int1.getValue(), int1.isPrime());
        System.out.printf("%d is prime? %s%n", int2.getValue(), int2.isPrime());
        System.out.printf("%d is prime? %s%n", int3.getValue(), int3.isPrime());
        System.out.printf("%d is even? %s%n", int1.getValue(), int1.isEven());
        System.out.printf("%d is even? %s%n", int2.getValue(), int2.isEven());
        System.out.printf("%d is even? %s%n", int3.getValue(), int3.isEven());
        System.out.printf("93 is odd? %s%n", MyInteger.isOdd(93));
        System.out.printf("%d equals %d? %s%n", int1.getValue(), int4.getValue(), int1.equals(int4));
        System.out.printf("%d%n", MyInteger.parseInt(new char[] {'1', '5', '6'}));
        System.out.printf("%d%n", MyInteger.parseInt("454"));
    }
}
class MyInteger {
    private int mVal;
    public MyInteger(int val) {
        mVal = val;
    }
    public int getValue() {
        return mVal;
    }
    public boolean isEven() {
        return (mVal % 2) == 0;
    }
    public boolean isOdd() {
        return (mVal % 2) == 1;
    }
    public boolean isPrime() {
        if (mVal == 1 || mVal == 2) {
            return true;
        }
        else {
            for (int i = 2; i < mVal; i++) {
                if (i % mVal == 0) return false;
            }
        }
        return true;
    }
    public static boolean isEven(int mint) {
        return (mint % 2) == 0;
    }
    public static boolean isOdd(int mint) {
        return (mint % 2) == 1;
    }
    public static boolean isPrime(int mint) {
        if (mint == 1 || mint == 2) {
            return true;
        }
        else {
            for (int i = 2; i < mint; i++) {
                if (i % mint == 0) return false;
            }
        }
        return true;
    }    
    public static boolean isEven(MyInteger mint) {
        return mint.isEven();
    }
    public static boolean isOdd(MyInteger mint) {
        return mint.isOdd();
    }
    public static boolean isPrime(MyInteger mint) {
        return mint.isPrime();
    }
    public boolean equals(int testInt) {
        if (testInt == mVal) 
            return true;
        return false;
    }
    public boolean equals(MyInteger mint) {
        if (mint.mVal == this.mVal) 
            return true;
        return false;
    }
    public static int parseInt(char[] values) {
        int sum = 0;
        for (char i : values) {
            sum += Character.getNumericValue(i);
        }
        return sum;
    }
    public static int parseInt(String value) {
        return Integer.parseInt(value);
    }
}