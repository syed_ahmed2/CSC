package homework.hw3;

public class Assignment03_Q05 {
    public static void main(String[] args) {
        Time time = new Time(555550000);
        Time time2 = new Time();
        Time time3 = new Time(5,23,55);
        System.out.printf("%d:%d:%d%n%n",time2.getHour(),time2.getMinute(),time2.getSecond());
        System.out.printf("%d:%d:%d%n%n",time.getHour(),time.getMinute(),time.getSecond());
        System.out.printf("%d:%d:%d%n%n",time3.getHour(),time3.getMinute(),time3.getSecond());
    }
}

class Time {
    private int milliHour;
    private int milliMinute;
    private int milliSecond;
    private long milliTime;

    public Time() {
        milliTime = System.currentTimeMillis();
    }

    public Time(long time) {
        milliTime = time;
    }
    public Time(int hour, int minute, int second) {
        milliHour = hour;
        milliMinute = minute;
        milliSecond = second;
    }

    public void setTime(long elapsedTime) {
        milliTime = elapsedTime;
    }


    public int getHour() {
        return (int)(milliTime / (1000 * 60 * 60)) % 24;
    }

    public int getMinute() {
        return (int)(milliTime / (1000 * 60)) % 60;
    }

    public int getSecond() {
        return (int)(milliTime / 1000) % 60;
    }

}