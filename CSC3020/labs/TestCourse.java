package labs;

class Course {
  private String courseName;
  private String[] students = new String[3];
  private int numberOfStudents;

  public Course(String courseName) {
      this.courseName = courseName;
      this.numberOfStudents = 0;
  }
  public String getCourseName() {
      return courseName;
  }
  public int getNumberOfStudents() {
      return numberOfStudents;
  }
  public String[] getStudents() {
      String[] arr = new String[numberOfStudents];
      for(int i=0;i<numberOfStudents;i++) {
          arr[i] = students[i];
      }
      return arr;
  }
  public void addStudent(String student) {
      if(numberOfStudents == students.length) { 
          String[] adder = new String[numberOfStudents];
          for(int i=0;i<numberOfStudents;i++) {
              adder[i] = students[i];
          }
          students = new String[numberOfStudents + 10];
          for(int i=0;i<numberOfStudents;i++) {
              students[i] = adder[i];
          }
      }
      students[numberOfStudents] = student;
      numberOfStudents++;
  }
  public void dropStudent(String student) {
      int idx = 0;
      int sumst = numberOfStudents;
      for(int i=0;i<sumst;i++) {
          if(students[i].equals(student)) {
              numberOfStudents--;
          }
          else {
              students[idx] = students[i];
              idx++;
          }
      }
  }
  
}