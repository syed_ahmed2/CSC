package labs;
import java.util.*;
import java.io.*;
public class lab07 {
    public static void main(String [] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter text file name: ");
        String input = s.nextLine();
        File file = new File(input);
        int X = 0;
        try {
            FileWriter M = new FileWriter("letterCount.txt");
            int Z;
            for (char a : "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray()) {
                BufferedReader R = new BufferedReader(new FileReader(file));
                char C = a;
                X = 0;
                while ((Z = R.read()) != -1) {
                    if (a == Character.toUpperCase((char) Z)) {
                        X++;
                    }
                }
                System.out.println("The occurrence of " + C + " is " + X);
                M.write("The occurrence of " + C + " is " + X + "\n");
            }
            M.close();    
        } catch (FileNotFoundException e) {
            System.out.println("FileNotFoundException error occurred.");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IOException error occurred.");
            e.printStackTrace();
        }
    
}
}