package labs;
import java.util.*;
public class lab03 {
    public static void main(String[] args) {
        Course course1 = new Course("course1");
        course1.addStudent("first");
        course1.addStudent("second");
        course1.addStudent("third");
        course1.addStudent("fourth");
        course1.addStudent("fifth");
        System.out.println("# of students: "+course1.getNumberOfStudents());
        System.out.println(Arrays.toString(course1.getStudents()));
        course1.dropStudent("first");
        System.out.println("updated student list: "+course1.getNumberOfStudents());
        System.out.println(Arrays.toString(course1.getStudents()));
        
}
}
