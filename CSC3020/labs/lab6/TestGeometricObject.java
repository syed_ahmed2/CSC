package labs.lab6;
import java.lang.reflect.Array;
import java.util.*;



public class TestGeometricObject {
static int search(ArrayList<GeometricObject> arr, GeometricObject ele) {
  int idx = 0;
  for (GeometricObject i : arr) {

    if (i.equals(ele)) {
      return idx;
    }
    idx++;
  }
  return -1;
}
  public static void main(String[] args) {

    GeometricObject[] arr =  new GeometricObject[4];
    ArrayList<GeometricObject> arr2 = new ArrayList<GeometricObject>();

    arr[0] = new Circle(5);
    arr[1] = new Circle(6);
    arr[2] = new Rectangle(2,3);
    arr[3] = new Rectangle(3,4);
    
    arr2.add(new Rectangle(10,20));
    arr2.add(new Rectangle(20,30));
    arr2.add(new Circle(5));
    arr2.add(new Circle(15));
    

     for (int i = 0; i < arr.length; i++) {
       System.out.printf("%.1f", arr[i].getPerimeter());
       System.out.println("\n");
     }
      Arrays.sort(arr);

 
     for (int i = 0; i < arr.length; i++) {
       System.out.printf("%.1f", arr[i].getPerimeter());
       System.out.println("\n");
     }
     System.out.println("\n");
 
 
     int index = search(arr2, new Circle(15));
     if (index == -1)
       System.out.println("not there");
     else
       System.out.println("found: " + index);
 
     index = search(arr2, new Rectangle(30,40));
     if (index == -1){
       System.out.println("not there");
     }else{
       System.out.println("found: " + index);
     }
 
   }
  }

