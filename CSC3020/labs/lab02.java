package labs;
import java.util.Scanner;
import java.util.Random;
import java.util.Date;
public class lab02 {
	public static void main(String[] args) {
		double[][] matrix = get();
            if(isMarkovMatrix(matrix)) {
                System.out.println("is a markov matrix");
            } else {
                System.out.println("not a markov matrix");

            }
	}
	public static double[][] get() {
        Scanner scan = new Scanner(System.in);
        Date s = new Date();
        Random r = new Random(s.getTime());
        int n = r.nextInt(2) + 2;
        double[][] input = new double[n][n];

        System.out.println("Enter a " + n + " by " + n + " matrix row by row:");

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                input[i][j] = scan.nextDouble();
            }
            System.out.println();
        }
        System.out.println();
		return input;
	}
	public static boolean isMarkovMatrix(double[][] m) {
		return pos(m) && sum(m);
	}
	public static boolean pos(double[][] m) {
		for (int i = 0; i < m.length; i++) {
			for (int j = 0; j < m[i].length; j++) {
				if (m[i][j] < 0)
					return false;
			}
		}
		return true;
	}
	public static boolean sum(double[][] m) {
		for (int i = 0; i < m.length; i++) {
			double sum = 0;
			for (int j = 0; j < m.length; j++) {
				sum += m[j][i];
			}
			if (sum == 1)
				return true;
		}
		return false;
	}
}