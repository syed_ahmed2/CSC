package labs;
public class testAccount {
    public static void main(String[] args) {
        CheckingAccount checkingAccount = new CheckingAccount(1, 35, 25.0);
        SavingsAccount savingsAccount = new SavingsAccount(2, 25, 0.05);

        System.out.println("Withdraw");
        System.out.println(checkingAccount.toString());
        System.out.println(savingsAccount.toString());

        checkingAccount.withdraw(10);
        savingsAccount.withdraw(10);

        System.out.println("Withdraw");
        System.out.println(checkingAccount.toString());
        System.out.println(savingsAccount.toString());
    }
}



class SavingsAccount extends Account {
    private float interest;
    
    public SavingsAccount(float interest, int id, double balance) {
        super(id, balance);
        this.interest = interest;
    }
    @Override
    public String toString() {
        return "Saving Account\n"+"Interest Rate: "+interest+"\n"+super.toString()+"\n";
    }

}

class CheckingAccount extends Account {
    private double fee;

    public CheckingAccount(double fee, int id, double balance) {
        super(id, balance);
        this.fee = fee;


    }
    @Override
    public String toString() {
        return "Checking Account\n"+"Monthly fee: "+fee+"\n"+super.toString()+"\n";
    }

}