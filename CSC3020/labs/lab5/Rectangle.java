package labs.lab5;

import java.lang.Math;
public class Rectangle extends GeometricObject {
  private double width;
  private double height;

  public Rectangle() {
  }

  public Rectangle(double width, double height) {
    this.width = width;
    this.height = height;
  }

  /** Return width */
  public double getWidth() {
    return width;
  }

  /** Set a new width */
  public void setWidth(double width) {
    this.width = width;
  }

  /** Return height */
  public double getHeight() {
    return height;
  }

  /** Set a new height */
  public void setHeight(double height) {
    this.height = height;
  }


  public double getArea() {
    return width * height;
  }


  public double getPerimeter() {
    return 2 * (width + height);
  }

  public double getDiagonal(){
    return Math.pow(width * width + height + height, 0.5);
  }
}