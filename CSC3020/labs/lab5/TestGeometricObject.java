package labs.lab5;
import java.util.*;
public class TestGeometricObject {
  public static void main(String[] args) {
    GeometricObject[] arr = new GeometricObject[4];
    arr[0] = new Circle(4);
    arr[1] = new Circle(3);
    arr[2] = new Rectangle(1,3);
    arr[3] = new Rectangle(1,3);

    sumArea(arr);
}
public static void sumArea(GeometricObject[] arr){
    double circleArea = 0, rectangleArea =0, diagLength = 0;
    for(GeometricObject o:arr){
        if(o instanceof Circle) {
            circleArea += ((Circle) o).getArea();
        }
        else if(o instanceof Rectangle){
            rectangleArea+= ((Rectangle) o).getArea();
            Rectangle RECT = (Rectangle) o;
            diagLength+=Math.sqrt(RECT.getWidth()*RECT.getWidth() + RECT.getHeight()*RECT.getHeight());
        }
    }
    System.out.println("circle area "+String.format("%.2f",circleArea));
    System.out.println("rectangle area "+ String.format("%.2f", rectangleArea));
    System.out.println("diagonal sum "+String.format("%.2f", diagLength));

}

}
