package project;

import java.util.*;
import java.io.*;
public class Main {
  // initialize variable for getting input
  static Scanner scan = new Scanner(System.in);

  public static void main(String[] args) {
    System.out.print("PID: ");
    // get patient ID as input
    int pid = scan.nextInt();
    System.out.print("patient first name: ");
    //get patient first name as input
    String fn = scan.next();
    System.out.print("patient last name: ");
    // get patient last name as input
    String ln = scan.next();
    // get doctor first name as input
    System.out.print("doctor first name: ");
    String docname = scan.next();
    // get doctor last name as input
    System.out.print("doctor last name: ");
    String doclname = scan.next();
    // get doctor specialty as input
    System.out.print("doctor specialty: ");
    String spy = scan.next();

    // create doctor objoct from inputted variables
    Doctor doc = new Doctor(docname, doclname, spy);
    // create patient object from inputted variables and variables that have already been set
    Patient P = new Patient(fn, ln, pid, new Date(30, 3, 2002), doc, new Date(3, 3, 2003), new Date(3, 2, 2010));
    // create bill object from input and variables that have already been set
    Bill B = new Bill(P.getPID(), 305.5, 3000, 920.35);

    System.out.println(P);
    System.out.println();
    System.out.println(B);
    //Name file after patient
    String filename = P.getfn() + P.getln() + ".txt";

    try { 
      //write to .txt file named after patient with all of the information we collected
      FileWriter X;
      X = new FileWriter(new File(filename));
      X.write(P.toString() + "\n");
      X.write("\n" + B);

      X.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
