package project;

public class Patient extends Person {
  //initialize variables
    private int PID;
    private Date dob;
    private Doctor phys;
    private Date aD;
    private Date dD;
  // Creates the Patient object with fn, ln, PID, dob, phys, aD, dD parameters
    Patient(String fn, String ln, int PID, Date dob, Doctor phys, Date aD, Date dD) {
      super(fn, ln);
      this.PID = PID;
      this.dob = dob;
      this.phys = phys;
      this.aD = aD;
      this.dD = dD;
    }
    // sets ID of patient
    public void setPID(int patientID) {
      this.PID = patientID;
    }
    // sets patient birthday
    public void setDob(Date dob) {
      this.dob = dob;
    }
  
    //sets which physician is in
    public void setPhys(Doctor physician) {
      this.phys = physician;
    }
  
    //sets which date is put in
    public void setaD(Date aD) {
      this.aD = aD;
    }
    //sets which date patient is released
    public void setdD(Date dD) {
      this.dD = dD;
    }
  
    // returns the ID of patient
    public int getPID() {
      return PID;
    }
  
    // returns patient birthday
    public Date getDob() {
      return dob;
    }
 
    //returns the current physician
    public Doctor getPhys() {
      return phys;
    }
  
    //returns date patient was put in
    public Date getaD() {
      return aD;
    }
    // returns date patient was released
    public Date getdD() {
      return dD;
    }
  // override toString method to print out desired output.
    @Override
    public String toString() {
      String p = "Patient: " + super.toString() + "\nDOB: " + dob + "\nPhysician: " + phys + "\nAdmit Date: " + aD + "\nDischarge Date: " + dD;
      return p;
    }
  }
