package project;

public class Bill {
  //initialize variables
    private int PID;
    private double PC;
    private double RC;
    private double DF;
    //creates bill object with PID, PC, RC and DF parameters
    Bill(int PID, double PC, double RC, double DF) {
      this.PID = PID;
      this.PC = PC;
      this.RC = RC;
      this.DF = DF;
    }
  
    //sets patient ID
    public void setPID(int PID) {
      this.PID = PID;
    }
  
// sets pharmacy charges
    public void setPC(double PC) {
      this.PC = PC;
    }
// sets doctor fees
    public void setDF(double DF) {
      this.DF = DF;
    }
  
// sets room charges
    public void setRC(double RC) {
      this.RC = RC;
    }
  //  returns patient ID
    public int getPID() {
      return PID;
    }
//  returns pharmacy charges
    public double getPC() {
      return PC;
    }
// returns room charges
    public double getRC() {
      return RC;
    }
  
// reurns doctor fees
    public double getDF() {
      return DF;
    }
  //  string method overrided to print the output we want
    @Override
    public String toString() {
      String b = "Pharmacy Charges: $" + PC + "\nRoom Charges: $" + RC + "\nDoctor Fees: $" + DF + "\n-------------\n" + "Total Charges: " + (PC + RC + DF);
      return b;
    }
  }
