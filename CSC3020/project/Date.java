package project;

public class Date {
  //initialize variables
    private int D;
    private int M;
    private int Y;
  
    Date(int D, int M, int Y) {
      // Checks if month input is correct
      if (M > 0 && M <= 12) {
        this.M = M;
      } else {
        System.out.println("illegal value");
      }
      // Checks if day input is correct
      if (D > 0 && D <= 31) {
        this.D = D;
      } else {
        System.out.println("illegal value");      
      }
  
      this.Y = Y;
    }
    // Checks if day input is correct
    public void setDay(int D) {
     if (D > 0 && D <= 31) {
        this.D = D;
      } else {
        System.out.println("illegal value");      
      }
    }
  
// Checks if month input is correct
    public void setMonth(int month) {
      if (month > 0 && month <= 12) {
        this.M = month;
      } else {
        System.out.println("illegal value");      
      }
    }
  
// takes year input, no checking
    public void setYear(int year) {
      this.Y = year;
    }
  
    @Override
    public String toString() {
      return M + "-" + D + "-"+ Y;
    }
  }
