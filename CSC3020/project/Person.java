package project;

public class Person {
  //initialize variables
    private String fn;
    private String ln;
  
    //Creates Person object with fn and ln paramters
    Person(String fn, String ln) {
      this.fn = fn;
      this.ln = ln;
    }
  
    //sets first name of person
    public void setfn(String fn) {
      this.fn = fn;
    }
    //sets last name of person
    public void setln(String ln) {
      this.ln = ln;
    }
  
    //returns first name of person
    public String getfn() {
      return fn;
    }
  
    //returns last name of peron
    public String getln() {
      return ln;
    }
    //toString method overrided to print output we want
    public String toString() {
      return fn + " " + ln;
    }
  }
