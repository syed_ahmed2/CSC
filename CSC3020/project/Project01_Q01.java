package project;
import java.util.*;
public class Project01_Q01 {

    public static void main(String[] args) {
        // get input from user and initialize variables.
        Scanner I = new Scanner(System.in);
        int y;
        int x;
        System.out.print("rows: ");
        x = I.nextInt();
        System.out.print("columns: ");
        y = I.nextInt();
        // 2D array initialized
        System.out.println("matrix values: ");
        int[][] m = new int[x][y];
        // for loop gets input for all of the matrix values.
        for (int i = 0; i < m.length; i++)
            for (int j = 0; j < m[i].length; j++) m[i][j] = I.nextInt();

        // tells if number is used 4 consecutive times or not
        System.out.println("Consecutive fours?:" +isConsecutiveFour(m));


    }

    public static boolean isConsecutiveFour(int[][] values) {
        // first loop checks first set of values in array and evaluates what to do if values are equal
        for (int z = 0; z < values.length; z++) {
            int C = values[z][0];
            int CC = 0;

            for (int k = 0; k < values[z].length; k++) {

                if (values[z][k] == C) {
                    CC++;
                    if (CC == 4) return true;
                } else {
                    C = values[z][k];
                    CC = 1;
                }
            }
        }
        // second loop checks second set of values in array and evaluates what to do if values are equal
        for (int z = 0; z < values[0].length; z++) {
            int C = 0;
            int CC = values[0][z];

            for (int i = 0; i < values.length; i++) {

                if (values[i][z] == CC) {
                    C++;
                    if (C == 4) return true;
                } else {
                    CC = values[i][z];
                    C = 1;
                }

            }
        }
        // third loop checks third set of values in array and evaluates what to do if values are equal
        for (int i = values.length - 1; i > 0; i--) {
            int y = i;
            int x = 0;
            int C = 0;
            int CC = values[y][x];

            while (y >= 0) {
                if (values[y][x] == CC) {
                    C++;
                    if (C == 4) return true;
                } else {
                    C = 1;
                    CC = values[y][x];
                }
                x++;
                y--;
            }
        }
                // fourth loop checks fourth set of values in array and evaluates what to do if values are equal

        for (int j = 0; j < values[0].length; j++) {
            int y = values.length - 1;
            int x = j;
            int C = 0;
            int CC = values[y][x];

            while (x < values[0].length && y >= 0) {
                if (values[y][x] == CC) {
                    C++;
                    if (C == 4) return true;
                } else {
                    C = 1;
                    CC = values[y][x];
                }
                x++;
                y--;
            }

        }
                // fifth loop checks fifth set of values in array and evaluates what to do if values are equal

        for (int j = values[0].length - 1; j > 0; j--) {

            int x = j;
            int y = values.length - 1;
            int C = values[y][x];
            int CC = 0;

            while (x >= 0 && y >= 0) {

                if (values[y][x] == C) {
                    CC++;
                    if (CC == 4) return true;
                } else {
                    CC = 1;
                    C = values[y][x];
                }

                x--;
                y--;
            }
        }
                // sixth loop checks sixth set of values in array and evaluates what to do if values are equal

        for (int R = 1 ; R < values.length; R++) {
            int x = values[0].length - 1;
            int y = R;
            int C = 0;
            int CC = values[y][x];

            while (y >= 0) {
                if (values[y][x] == CC) {
                    C++;
                    if (C == 4) return true;
                } else {
                    C = 1;
                    CC = values[y][x];
                }
                x--;
                y--;
            }
        }
        return false;
    }
}
