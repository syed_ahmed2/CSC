package project;

public class Doctor extends Person {
  //initialize variables
    private String spy;
  
  // Creates the Doctor object with fn, ln, spy parameters
    Doctor(String fn, String ln, String spy) {
      super(fn, ln);
      this.spy = spy;
    }
    
    //sets the spy variable
    public void setspy(String spy) {
      this.spy = spy;
    }
    //returns the spy variable for the program to use
    public String getspy() {
      return spy;
    }
  // override toString() to print what we actually want
    @Override
    public String toString() {
      return super.toString() + " " + spy;
    }
  }
