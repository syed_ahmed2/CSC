package project.Q3;
public class CruiseShip extends Ship implements Comparable<CruiseShip>{
    int MP;


    // creates cruiseship object with parameters, P,N, and Y
    public CruiseShip(int P, String N, String Y) {
        super(N, Y);
        this.MP = P;
    }
// sets max amount of passengers
    public void setMP(int MP) {
        this.MP = MP;
    }
// returns max amount of passengers
    public int getMP() {
        return MP;
    }
// overrides toString() method to print what we want.
    @Override
    public String toString() {
        return super.toString() + " MP: " + MP;
    }
// checks if cruiseship is the same or not
    public int compareTo(CruiseShip other) {
        return this.MP - other.MP;
    }
}
