
package project.Q3;
public class TankerShip extends Ship {
    // initialzie variables
    String gas;
    double captonnage;
// creates tankership object with gas, captonnage, N, and Y parameters
    public TankerShip(String gas, double captonnage, String N, String Y) {
        super(N, Y);
        this.gas = gas;
        this.captonnage = captonnage;
    }
// sets capacity in tonnage
    public void setcaptonnage(double captonnage) {
        this.captonnage = captonnage;
    }
// returns capacity in tonnage
    public double getcaptonnage() {
        return captonnage;
    }
// sets gas type
    public void setgas(String gas) {
        this.gas = gas;
    }
// returns gas type
    public String getgas() {
        return gas;
    }
//overrides string method to return what we want
    @Override
    public String toString() {
        String X = super.toString() + ", gas: " + gas + ", captonnage: " + captonnage;
        return X;
    }


//checks if has type is equal for ships    
    public boolean compareTo(TankerShip other) {
        return this.gas.equals(other.gas);
    }
}
