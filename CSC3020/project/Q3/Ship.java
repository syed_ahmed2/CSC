package project.Q3;
public class Ship {
    //initialize variables
    String N;
    String Y;
    //creates ship object with N and Y parameters
    public Ship(String N, String Y) {
        this.N = N;
        this.Y = Y;
    }
    // sets name of ship
    public void setName(String N) {
        this.N = N;
    }
    // sets year ship was built
    public void setYear(String Y) {
        this.Y = Y;
    }
    // returns name of ship
    public String getName() {
        return N;
    }
    // retirms year ship was built
    public String getYear() {
        return Y;
    }
    //overrides toString() method with output we want
    @Override
    public String toString() {
        String p = "name: " + N + ", year: " + Y;
        return p;
    }


}