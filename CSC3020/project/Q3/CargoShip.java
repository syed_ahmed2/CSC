package project.Q3;

public class CargoShip extends Ship implements Comparable<CargoShip>{
    // initialzie variables
    int tons;
    // checks if ship capacity is same in tons
    @Override
    public int compareTo(CargoShip other) {
        return this.tons - other.tons;
    }
    // creates cargoship object with tons, N, and Y parameters
    public CargoShip(int tons, String N, String Y) {
        super(N, Y);
        this.tons = tons;
    }

    // sets capacity of ship in tons
    public void settons(int tons) {
        this.tons = tons;
    }
    // returns capacity of ship in tons
    public int gettons() {
        return tons;

    }

    //override toString method to print what we want
    @Override
    public String toString() {
        return super.toString() + ", tons: " + tons; 
    }

}