package project.Q3;
import java.util.*;
public class project01_Q03 {
    public static void main(String[] args) {
        
        //declare array for ships
        ArrayList<Ship> arr = new ArrayList<Ship>();


        //declaring objects for each type of ship and then adding them to the
        //array of ships
        Ship S1 = new Ship("S1", "1935");
        arr.add(S1);
        CruiseShip S2 = new CruiseShip(1000, "S2", "2534");
        arr.add(S2);
        CargoShip S3 = new CargoShip(4600, "S3", "3445");
        arr.add(S3);
        TankerShip S4 = new TankerShip("gas", 4354.56, "S4", "2304");
        arr.add(S4);
        // Prints out the ships listed aboce from the array
        for (int i = 0; i < arr.size(); i++) {
            String F = arr.get(i).toString();
            System.out.println(F);
        }
        System.out.println("\n");
        System.out.println("g:");
        // Declare separate array for cargo ships and then create cargoship
        // objects.
        ArrayList<CruiseShip> CR = new ArrayList<CruiseShip>();
        CruiseShip C1 = new CruiseShip(500, "C1", "2001");
        CR.add(C1);
        CruiseShip C2 = new CruiseShip(300, "C2", "2015");
        CR.add(C2);
        CruiseShip C3 = new CruiseShip(100, "C3", "2000");
        CR.add(C3);
        System.out.println("before sort: ");
        // prints out ships before they are sorted to be in order
        for (int i = 0; i < CR.size(); i++) {
            String K = CR.get(i).toString();
            System.out.println(K);
        }
        Collections.sort(CR);
        System.out.println("\n");
        System.out.println("after sort:");
        // prints out ships after they are sorted to be in order
        for (int i = 0; i < CR.size(); i++) {
            String G = CR.get(i).toString();
            System.out.println(G);
        }


    }
}
