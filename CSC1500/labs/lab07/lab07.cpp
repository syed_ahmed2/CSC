#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>     
using namespace std;

int bubblesort(int *arr, int l);
void print(int arr[], int l);

int bubblesort(int *arr, int l)
{
    int a = 0;
    for (int i = 0; i < l; i++)
    {
        for (int j = 1; j < (l - i); j++)
        {
            a += 1;
            if (arr[j - 1] > arr[j])
            {
                a += 3;
                int temp = arr[j - 1];
                arr[j - 1] = arr[j];
                arr[j] = temp;
            }
        }
    }
  
    return a;
}

void print(int arr[], int l)
{
    cout << "(" << arr[0];
    for (int i = 1; i < l; i++)
    {
        cout << "," << arr[i];
    }
    cout << ")";
}

int main()
{
    int arr0[] = {1, 3, 7, 5, 2, 4, 6, 8, 9, 10};
    int arr1[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
    int arr2[] = {1, 10, 2, 9, 3, 8, 4, 7, 5, 6};
    int size = 0;
    int a = 0;
    size = sizeof(arr0) / sizeof(int);
    cout << "original arr1: ";
    print(arr0, size);
    cout << "\n";
    a = bubblesort(arr0, size);
    cout << "Sorted: ";
    print(arr0, size);
    cout << " " << a << " Actions" << endl;
    size = sizeof(arr1) / sizeof(int);
    cout << "original arr2: ";
    print(arr1, size);
    cout << "\n";
    a = bubblesort(arr1, size);
    cout << "Sorted: ";
    print(arr1, size);
    cout << " " << a << " Actions" << endl;
    size = sizeof(arr2) / sizeof(int);
    cout << "original arr3: ";
    print(arr2, size);
    cout << "\n";
    a = bubblesort(arr2, size);
    cout << "Sorted: ";
    print(arr2, size);
    cout << " " << a << " Actions" << endl;  
}