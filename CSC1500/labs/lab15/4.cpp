#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
int main ()
{
int setA[] = {1,2,3,5,8,13};
int setB[] = {1,3,5,7,9,11,13,15,17,19};
int setC[] = {1,2,3,5,7,11,13,17,19};
int unionArray[50], size = 0;
vector<int> unionSet(20);
vector<int> intersectSet(20);
vector<int>::iterator st;
st = set_union(setA, setA + 6, setB, setB + 10, unionSet.begin());
unionSet.resize(st - unionSet.begin());
for (st = unionSet.begin(); st != unionSet.end(); ++st)
unionArray[size++] = *st;
st = set_intersection(unionArray, unionArray + size, setC, setC + 10, intersectSet.begin());
intersectSet.resize(st - intersectSet.begin());
cout<<"The resultant set is: "<<endl;
for (st = intersectSet.begin(); st != intersectSet.end(); ++st)
cout<< *st<<" ";
return 0;
}