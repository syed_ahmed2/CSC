#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>     
#include <bits/stdc++.h>
using namespace std;    

string convert(int x, int y){
    string z;
    char k;

    do{
        k = x % y;
        
        if(k < 10)
            k += '0';
        else
            k = k + 'A' - 10;

        z += k;

        x = x/y;    

    } while(x > 0);

    reverse(z.begin(),z.end());

    return z;
}

int main(){
    int y;
    int z;
    cout << "number:";
    cin >> y;
    cout << "base:";
    cin >> z;
    string x = convert(y, z);

    cout << y << " is " << x << " in base " << z << endl;
}
