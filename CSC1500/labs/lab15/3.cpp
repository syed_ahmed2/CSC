#include<bits/stdc++.h>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>     
#include <bits/stdc++.h>
using namespace std;
int main() {
cout << "input what to convert: ";
string user_input;
getline(cin, user_input);
int hamming_code[8];
hamming_code[3] = (user_input[0] - '0');
hamming_code[5] = (user_input[1] - '0');
hamming_code[6] = (user_input[2] - '0');
hamming_code[7] = (user_input[3] - '0');
hamming_code[1] = (hamming_code[3] ^ hamming_code[5] ^ hamming_code[7]);
hamming_code[2] = (hamming_code[3] ^ hamming_code[6] ^ hamming_code[7]);
hamming_code[4] = (hamming_code[5] ^ hamming_code[6] ^ hamming_code[7]);
cout << "hamming code output: ";
for (int i=1; i<8; i++)
cout << hamming_code[i] << " ";
if(hamming_code[7] == 0)
hamming_code[7] = 1;
else
hamming_code[7] = 0;
cout << endl;
cout << "changing 7th bit : ";
for (int i=1; i<8; i++)
cout << hamming_code[i] << " ";
int p1 = hamming_code[1] ^ hamming_code[3] ^ hamming_code[5] ^ hamming_code[7];
int p2 = hamming_code[2] ^ hamming_code[3] ^ hamming_code[6] ^ hamming_code[7];
int p3 = hamming_code[4] ^ hamming_code[5] ^ hamming_code[6] ^ hamming_code[7];
int parity = p1 + (p2 * 2) + (p3 * 4);
if(parity == 0)
cout << "\nno error" << endl;
else
cout << "\nerror at " << parity << endl;
if(hamming_code[parity] == 0)
hamming_code[parity] = 1;
else
hamming_code[parity] = 0;
cout << "code after fixing error: ";
int org_msg = hamming_code[7] + (hamming_code[6] * 2) + (hamming_code[5] * 4) +
(hamming_code[3] * 8);
cout << org_msg << endl;
}