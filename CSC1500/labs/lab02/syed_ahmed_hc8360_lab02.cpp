#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;

int main() {
    
    string array[8] = {"000", "001", "010", "011", "100", "101", "110", "111"};
    int i = 0;
    int x;
    int y;
    int z;
    
    while (i <= 7)
    {
        x = int(array[i].at(0))-48;
        y = int(array[i].at(1))-48;
        z = int(array[i].at(2))-48;
        
        int num1 = (x && y) or (x && z);
        cout << num1;
        i++;
    }
    cout << "\n";
    i = 0;
    while (i <= 7)
    {
        x = int(array[i].at(0))-48;
        y = int(array[i].at(1))-48;
        z = int(array[i].at(2))-48;
        
        int num2 = (x && z) and (y && !z);
        cout << num2;
        i++;
    }
    cout << "\n";
    i = 0;
    while (i <= 7)
    {
        x = int(array[i].at(0))-48;
        y = int(array[i].at(1))-48;
        z = int(array[i].at(2))-48;
        
        int num3 = ( x || y) and !(y || z);
        cout << num3;
        i++;
    }
    cout << "\n";
    i = 0;
    while (i <= 7)
    {
        x = int(array[i].at(0))-48;
        y = int(array[i].at(1))-48;
        z = int(array[i].at(2))-48;
        
        int num4 = (x || (y && z)) and (!x && !y);
        cout << num4;
        i++;
    }
    cout << "\n";
    i = 0;
    while (i <= 7)
    {
        x = int(array[i].at(0))-48;
        y = int(array[i].at(1))-48;
        z = int(array[i].at(2))-48;
        
        int num5 = ((y && z) || (z && x)) and (!(x || y) && z);
        cout << num5;
        i++;
    }
    cout << "\n";
}