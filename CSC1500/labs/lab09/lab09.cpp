#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>     
using namespace std;

int U(int* set1, int m, int* set2, int n, int Result[]);
int IN(int* set1, int m, int* set2, int n, int Result[]);
int S(int* set1, int m, int* set2, int n, int Result[]);
int main(void)
{
    int length;
    int A[10] = { 1,2,3,4,5,6,7,8,9,10 };
    int B[5] = { 2,4,6,8,10 };
    int C[5] = { 1,3,5,7,9 };
    int D[5] = { 1,2,3,5,7 };
    int E[50];

    cout << "Problem 1:" << endl;
    length = IN(A, 10, D, 5, E);
    cout << "{ ";
    for (int i = 0; i < length; i++)
    {
        cout << E[i] << " ";
    }
    cout << "}" << endl;
    cout << "Problem 2:" << endl;
    length = U(B, 5, C, 5, E);
    length = IN(E, length, A, 10, E);
    cout << "{ ";

    for (int i = 0; i < length; i++)
    {
        cout << E[i] << " ";
    }

    cout << "}" << endl;
    cout << "Problem 3:" << endl;
    length = S(A, 10, C, 5, E);
    length = U(E, length, A, 10, E);
    length = IN(E, length, A, 10, E);

    cout << "{ ";

    for (int i = 0; i < length; i++)
    {
        cout << E[i] << " ";
    }

    cout << "}" << endl;
    cout << "Problem 4:" << endl;
    cout << "{ ";
    length = S(A, 10, D, 5, E);
    for (int i = 0; i < length; i++)
    {
        cout << E[i] << " ";
    }
    cout << "}" << endl;

    cout << "Problem 5:"<< endl;
    length = S(A, 10, A, 10, E);
    int temp[10];
    int size = U(C, 5, D, 5, temp);
    length = U(E, length, temp, size, E);
    cout << "Length: " << length << endl;



    cout << "Problem 6:"<< endl;
    length = IN(A, 10, B, 5, E);
    cout << "{ ";

    for (int i = 0; i < length; i++)
    {
        cout << E[i] << " ";
    }

    cout << "}" << endl;

    cout << "Problem 7:"<< endl;
    length = IN(A, 10, B, 5, E);
    cout << "Length: " << length << endl;

    cout << "Problem 8:" << endl;
    length = U(A, 10, B, 5, E);
    length = U(E, length, C, 5, E);
    length = U(E, length, D, 5, E);
    cout <<  "{ ";

    for (int i = 0; i < length; i++)
    {
        cout << E[i] << " ";
    }

    cout << "}" << endl;
}

int U(int* set1, int m, int* set2, int n, int Result[])
{
    int l = 0;
    int i, j, found;
    for (i = 0; i < m; i++)
    {
        Result[i] = set1[i];
        l++;
    }
    int e = l;
    for (i = 0; i < n; i++)
    {
        found = 0;
        for (j = 0; j < e; j++)
        {
            if (set2[i] == set1[j])
            {
                found = 1;
                break;
            }
        }
        if (found == 0)
        {
            Result[l] = set2[i];
            l++;
        }
    }
    return l;
}

int IN(int* set1, int m, int* set2, int n, int Result[])
{
    int i, j, l = 0;
    for (i = 0; i < m; i++)
    {
        for (j = 0; j < n; j++)
        {
            if (set1[i] == set2[j])
            {
                Result[l] = set1[i];
                l++;
            }
        }
    }
    return l;
}


int S(int* set1, int m, int* set2, int n, int Result[])
{
    int i, j, l = 0, found;
    for (i = 0; i < m; i++)
    {
        found = 0;
        for (j = 0; j < n; j++)
        {

            if (set1[i] == set2[j])
            {
                found = 1;
                break;
            }
        }
        if (found == 0)
        {
            Result[l] = set1[i];
            l++;
        }
    }

    for (i = 0; i < n; i++)
    {
        int found = 0;
        for (j = 0; j < m; j++)
        {
            if (set2[i] == set1[j])
            {
                found = 1;
                break;
            }
        }
        if (found == 0)
        {
            Result[l] = set2[i];
            l++;
        }
    }
    return l;
}