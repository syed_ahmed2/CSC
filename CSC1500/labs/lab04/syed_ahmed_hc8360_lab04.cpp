#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;

bool implies(bool A, bool B){
    return !(A and !B);
}

int main(){
    int rocket[16] = {0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1};
    int astronaut[16] = {0,0,0,0,1,1,1,1,0,0,0,0,1,1,1,1};
    int trained[16] = {0,0,1,1,0,0,1,1,0,0,1,1,0,0,1,1};
    int educated[16] = {0,1,0,1,0,1,0,1,0,1,0,1,0,1,0,1};

    for(int i = 0; i <= 15; i++){
        cout << setw(10) << rocket[i] << setw(10) << astronaut[i] << setw(10) << trained[i] << setw(15) << educated[i] << setw(15);
        if(implies(educated[i], rocket[i])){
            cout << "1" << endl;
        } else {
            cout << "0/INVALID" << endl;
        }
    }

}