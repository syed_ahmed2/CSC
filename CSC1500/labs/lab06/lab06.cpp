#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>     
using namespace std;

int main(){
    int x;
    int y = 0;
    int z;

    cout << "enter number: ";
    cin >> x;

    for(int i = 0; i <= x; i++){
        y += i;
        z = i-1;
    }
    cout <<"method 1 sums equals " << y << " done in " << z << " operations" << endl;

    y = 0;

    y = (x*(x+1))/2;
    z = 3;
    cout << "method 2 sums equals " << y << " done in " << z << " operations" << endl;
}