#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <cstring>
#include <string>
using namespace std;
bool implies(bool A, bool B) {
    return !(A and !B);
}

int main()
{
    bool A[] = { 0,0,0,0,1,1,1,1 };
    bool B[] = { 0,0,1,1,0,0,1,1 };
    bool C[] = { 0,1,0,1,0,1,0,1 };
    cout << "1 = True" << endl;
    cout << "0 = False" << endl;
    for (int x = 0; x <= 7; x++) {
        cout << endl;
        cout << endl;
        cout << "Index " << x << endl;
        cout << "\n(1):" << (!(A[x] || C[x]) ^ B[x]);
        cout << "\n(2):"<< !((implies(&B[x],&C[x])) && !(A[x] || B[x]));
        cout << "\n(3):"<< (implies((A[x] ^ B[x]),(!(B[x] ^ C[x]))) );
        cout << "\n(4):"<< (implies(implies(A[x],B[x]),!C[x])); 
    }
    cout << endl;
}