#include<iostream>
using namespace std;

void matrix_multiply(int a[2][2], int b[2][2]);
void matrix_addition(int a[2][2], int b[2][2]);
void scalar_multiply(int a[2][2], int n);
void matrix_display(int a[2][2]);

int main()
{
int a[2][2]={{2,0},{4,2}};
int b[2][2]={{1,3},{2,0}};
int c[2][2]={{1,0},{0,1}};
cout<<"Matrix A * Matrix B:\n";
matrix_multiply(a,b);
cout<<"Matrix B * Matrix A:\n";
matrix_multiply(b,a);
cout<<"Matrix C * Matrix B:\n";
matrix_multiply(c,b);
cout<<"Matrix A + Matrix B:\n";
matrix_addition(a,b);
cout<<"-4 * Matrix B:\n";
scalar_multiply(b,-4);
return 0;
}

void matrix_multiply(int a[2][2], int b[2][2])
{
int c[2][2];
for(int i=0;i<2;i++)
{
for(int j=0;j<2;j++)
{
c[i][j]=0;
for(int k=0;k<2;k++)
{
c[i][j] += a[i][k]*b[k][j];
}
}
}
matrix_display(c);
}
void matrix_addition(int a[2][2], int b[2][2])
{
int c[2][2];
for(int i=0;i<2;i++)
{
for(int j=0;j<2;j++)
{
c[i][j] = a[i][j] + b[i][j];
}
}
matrix_display(c);
}

void scalar_multiply(int a[2][2], int n)
{
int c[2][2];
for(int i=0;i<2;i++)
{
for(int j=0;j<2;j++)
{
c[i][j] = a[i][j] * n;
}
}
matrix_display(c);
}


void matrix_display(int a[2][2])
{
for(int i=0;i<2;i++)
{
for(int j=0;j<2;j++)
cout<<a[i][j]<<" ";
cout<<endl;
}
cout<<endl;
}