#include <iostream>
#include <random>
#include <vector>
#include <time.h>
using namespace std;

int DrawCard(); 
int GameWin(vector<int> playerCards, vector<int> dealerCards); 
bool DealerHit(vector<int> dealerCards); 
bool TwentyOne(vector<int> cards); 
bool Bust(vector<int> cards); 
void PrintGame(vector<int> playerCards, vector<int> dealerCards); 
void PrintFull(vector<int> playerCards, vector<int> dealerCards); 
void Probability(vector<int> playerCards, vector<int> dealerCards, int runs); 

int main()
{
   srand(time(NULL));
   bool quit = false;
   while (!quit)
   {
       int play;
       cout << "1 - play 0 - don't play: ";
       cin >> play;
       if (play == 1)
       {
           vector<int> playerCards;
           vector<int> dealerCards;

           dealerCards.push_back(DrawCard());
           dealerCards.push_back(DrawCard());
           playerCards.push_back(DrawCard());
           playerCards.push_back(DrawCard());

           bool gameOver = false;
           cout << endl << endl << "first deal:" << endl;
           while (!gameOver)
           {
               char hitStand;
               PrintGame(playerCards, dealerCards);
               if (Bust(playerCards))
               {
                   cout << endl << endl << "lost game" << endl << endl;
                   gameOver = true;
               }
               else if (TwentyOne(playerCards))
               {
                   cout << endl << endl << "dealer show hand" << endl << endl;
                   PrintFull(playerCards, dealerCards);
                   bool hit = DealerHit(dealerCards);
                   while (hit == true)
                   {
                       if (playerCards.size() != 2)
                       {
                           cout << endl << endl << "dealer hit" << endl << endl;
                           dealerCards.push_back(DrawCard());
                           hit = DealerHit(dealerCards);
                           PrintFull(playerCards, dealerCards);
                       }
                       else
                           hit = false;
                   }
                   if (TwentyOne(dealerCards))
                   {
                       if (playerCards.size() == 2 && dealerCards.size() == 2) 
                           cout << endl << endl << "blackjack (tie)." << endl << endl;
                       else if (playerCards.size() == 2 && dealerCards.size() != 2) 
                           cout << endl << endl << "blackjack win" << endl << endl;
                       else if (playerCards.size() != 2 && dealerCards.size() == 2) 
                           cout << endl << endl << "blackjack lost" << endl << endl;
                       else
                           cout << endl << endl << "same total. push (tie)." << endl << endl;
                       gameOver = true;
                   }
                   else
                   {
                       if (playerCards.size() == 2)
                           cout << endl << endl << "You have blackjack You win" << endl << endl;
                       else
                       {
                           if (Bust(dealerCards)) 
                               cout << endl << endl << "dealer busts";
                           else 
                               cout << endl << endl << "dealer stands";
                           cout << "You win" << endl << endl;
                       }
                       gameOver = true;
                   }
               }
               else
               {
                   Probability(playerCards, dealerCards, 1000);

                   cout << endl << endl << "What do you want to do? (H/S) ";
                   cin >> hitStand;
                   if (hitStand == 'H' || hitStand == 'h') 
                   {
                       cout << endl << "The player hits . . ." << endl;
                       playerCards.push_back(DrawCard());
                   }
                   else if (hitStand == 'S' || hitStand == 's') 
                   {
                       cout << endl << "The player stands and the dealer shows his hand . . ." << endl;
                       PrintFull(playerCards, dealerCards);
                       bool hit = DealerHit(dealerCards);
                       while (hit == true) 
                       {
                           cout << endl << endl << "The dealer hits . . ." << endl;
                           dealerCards.push_back(DrawCard());
                           hit = DealerHit(dealerCards);
                           PrintFull(playerCards, dealerCards);
                       }
                       if (TwentyOne(dealerCards)) 
                       {
                           if (dealerCards.size() == 2) 
                               cout << endl << endl << "I'm sorry. The dealer has blackjack and you lost the game." << endl << endl;
                           else 
                               cout << endl << endl << "I'm sorry. The dealer has hit 21 and you lost the game." << endl << endl;
                           gameOver = true;
                       }
                       else
                       {
                           if (Bust(dealerCards)) 
                               cout << endl << endl << "The dealer busts . . . Congratulations! You win!" << endl << endl;
                           else
                           {
                               cout << endl << endl << "The dealer stands . . . ";
                               int result = GameWin(playerCards, dealerCards);
                               if (result == 1)
                                   cout << "Congratulations! You win!" << endl << endl;
                               else if (result == 0)
                                   cout << "I'm sorry. You lost the game." << endl << endl;
                               else
                                   cout << "You both have the same total. This is a push (tie)." << endl << endl;
                           }
                           gameOver = true;
                       }
                   }
                   else
                       cout << endl << "Please specify H or S." << endl;
               }
           }
       }
       else if (play == 0) 
           quit = true;
       else 
           cout << endl << "I'm sorry, you must respond with \"1\" or \"0\"." << endl;
   }
}

int DrawCard()
{
   int cardNum;
   cardNum = (rand() % 13) + 1;
   return cardNum;
}

int GameWin(vector<int> playerCards, vector<int> dealerCards)
{
   int playerSum = 0;
   int dealerSum = 0;
   for (int i = 0; i < playerCards.size(); i++)
   {
       playerSum += playerCards[i];
       if (playerCards[i] == 11)
           playerSum -= 1;
       else if (playerCards[i] == 12)
           playerSum -= 2;
       else if (playerCards[i] == 13)
           playerSum -= 3;
   }
   for (int i = 0; i < playerCards.size(); i++)
   {
       if (playerCards[i] == 1 && playerSum <= 11)
           playerSum += 10; 
   }
   for (int i = 0; i < dealerCards.size(); i++)
   {
       dealerSum += dealerCards[i];
       if (dealerCards[i] == 11)
           dealerSum -= 1;
       else if (dealerCards[i] == 12)
           dealerSum -= 2;
       else if (dealerCards[i] == 13)
           dealerSum -= 3;
   }
   for (int i = 0; i < dealerCards.size(); i++)
   {
       if (dealerCards[i] == 1 && dealerSum <= 11)
           dealerSum += 10; 
   }
   if (playerSum > dealerSum)
       return 1; 
   else if (dealerSum > playerSum)
       return 0; 
   else
       return -1; 
}

bool DealerHit(vector<int> dealerCards)
{
   int dealerSum = 0;
   for (int i = 0; i < dealerCards.size(); i++)
   {
       dealerSum += dealerCards[i];
       if (dealerCards[i] == 11)
           dealerSum -= 1;
       else if (dealerCards[i] == 12)
           dealerSum -= 2;
       else if (dealerCards[i] == 13)
           dealerSum -= 3;
   }
   for (int i = 0; i < dealerCards.size(); i++)
   {
       if (dealerCards[i] == 1 && dealerSum <= 11)
           dealerSum += 10; 
   }
   if (dealerSum > 16)
       return false;
   else
       return true;
}

bool TwentyOne(vector<int> cards)
{
   int sum = 0;
   for (int i = 0; i < cards.size(); i++)
   {
       sum += cards[i];
       if (cards[i] == 11)
           sum -= 1;
       else if (cards[i] == 12)
           sum -= 2;
       else if (cards[i] == 13)
           sum -= 3;
   }
   for (int i = 0; i < cards.size(); i++)
   {
       if (cards[i] == 1 && sum <= 11)
           sum += 10; 
   }
   if (sum == 21)
       return true;
   else
       return false;
}

bool Bust(vector<int> cards)
{
   int sum = 0;
   for (int i = 0; i < cards.size(); i++)
   {
       sum += cards[i];
       if (cards[i] == 11)
           sum -= 1;
       else if (cards[i] == 12)
           sum -= 2;
       else if (cards[i] == 13)
           sum -= 3;
   }
   for (int i = 0; i < cards.size(); i++)
   {
       if (cards[i] == 1 && sum <= 11)
           sum += 10; 
   }
   if (sum > 21)
       return true;
   else
       return false;
}

void PrintGame(vector<int> playerCards, vector<int> dealerCards)
{
   cout << "Dealer's Cards: (total #)\n\t#\t";
   for (int i = 1; i < dealerCards.size(); i++)
   {
       if (dealerCards[i] == 1)
           cout << "A\t";
       else if (dealerCards[i] == 11)
           cout << "J\t";
       else if (dealerCards[i] == 12)
           cout << "Q\t";
       else if (dealerCards[i] == 13)
           cout << "K\t";
       else
           cout << dealerCards[i] << "\t";
   }
   int playerSum = 0;
   for (int i = 0; i < playerCards.size(); i++)
   {
       playerSum += playerCards[i];
       if (playerCards[i] == 11)
           playerSum -= 1;
       else if (playerCards[i] == 12)
           playerSum -= 2;
       else if (playerCards[i] == 13)
           playerSum -= 3;
   }
   for (int i = 0; i < playerCards.size(); i++)
   {
       if (playerCards[i] == 1 && playerSum <= 11)
           playerSum += 10;
   }
   cout << endl << endl << "Player's Cards: (total " << playerSum << ")\n\t";
   for (int i = 0; i < playerCards.size(); i++)
   {
       if (playerCards[i] == 1)
           cout << "A\t";
       else if (playerCards[i] == 11)
           cout << "J\t";
       else if (playerCards[i] == 12)
           cout << "Q\t";
       else if (playerCards[i] == 13)
           cout << "K\t";
       else 
           cout << playerCards[i] << "\t";
   }
}

void PrintFull(vector<int> playerCards, vector<int> dealerCards)
{
   int dealerSum = 0;
   for (int i = 0; i < dealerCards.size(); i++)
   {
       dealerSum += dealerCards[i];
       if (dealerCards[i] == 11)
           dealerSum -= 1;
       else if (dealerCards[i] == 12)
           dealerSum -= 2;
       else if (dealerCards[i] == 13)
           dealerSum -= 3;
   }
   for (int i = 0; i < dealerCards.size(); i++)
   {
       if (dealerCards[i] == 1 && dealerSum <= 11)
           dealerSum += 10;
   }
   cout << "Dealer's Cards: (total " << dealerSum << ")\n\t";
   for (int i = 0; i < dealerCards.size(); i++)
   {
       if (dealerCards[i] == 1)
           cout << "A\t";
       else if (dealerCards[i] == 11)
           cout << "J\t";
       else if (dealerCards[i] == 12)
           cout << "Q\t";
       else if (dealerCards[i] == 13)
           cout << "K\t";
       else 
           cout << dealerCards[i] << "\t";
   }
   int playerSum = 0;
   for (int i = 0; i < playerCards.size(); i++)
   {
       playerSum += playerCards[i];
       if (playerCards[i] == 11)
           playerSum -= 1;
       else if (playerCards[i] == 12)
           playerSum -= 2;
       else if (playerCards[i] == 13)
           playerSum -= 3;
   }
   for (int i = 0; i < playerCards.size(); i++)
   {
       if (playerCards[i] == 1 && playerSum <= 11)
           playerSum += 10; 
   }
   cout << endl << endl << "Player's Cards: (total " << playerSum << ")\n\t";
   for (int i = 0; i < playerCards.size(); i++)
   {
       if (playerCards[i] == 1)
           cout << "A\t";
       else if (playerCards[i] == 11)
           cout << "J\t";
       else if (playerCards[i] == 12)
           cout << "Q\t";
       else if (playerCards[i] == 13)
           cout << "K\t";
       else 
           cout << playerCards[i] << "\t";
   }
}

void Probability(vector<int> playerCards, vector<int> dealerCards, int runs)
{
   int hitWins = 0; 
   int standWins = 0; 
   int hitDraws = 0; 
   int standDraws = 0; 

   cout << endl << endl << "Probability Simulation running . . . " << endl
       << "Playing " << runs << " games to determine probability of win if you stand or hit . . . " << endl;

   for (int i = 0; i < runs; i++)
   {
       vector<int> tempDealer; 
       tempDealer.push_back(dealerCards[1]); 
       vector<int> tempPlayer; 
       for (int j = 0; j < playerCards.size(); j++)
           tempPlayer.push_back(playerCards[j]);
       tempPlayer.push_back(DrawCard()); 
       if (!Bust(tempPlayer)) 
       {
           bool hit = DealerHit(tempPlayer);
           while (hit == true)
           {
               tempPlayer.push_back(DrawCard());
               hit = DealerHit(tempPlayer);
           }
           if (!Bust(tempPlayer))
           {
               hit = DealerHit(tempDealer);
               while (hit == true)
               {
                   tempDealer.push_back(DrawCard());
                   hit = DealerHit(tempDealer);
               }
               if (Bust(tempDealer))
                   hitWins++;
               else
               {
                   int win = GameWin(tempPlayer, tempDealer);
                   if (win == 1)
                       hitWins++;
                   else if (win == -1)
                       hitDraws++;
               }
           }
       }
   }
   for (int i = 0; i < runs; i++)
   {
       vector<int> tempDealer; 
       tempDealer.push_back(dealerCards[1]); 
       bool hit = DealerHit(tempDealer);
       while (hit == true)
       {
           tempDealer.push_back(DrawCard());
           hit = DealerHit(tempDealer);
       }
       if (Bust(tempDealer))
           standWins++;
       else 
       {
           int win = GameWin(playerCards, tempDealer);
           if (win == 1)
               standWins++;
           else if (win == -1)
               standDraws++;
       }
   }
   cout << "Probability hit resulted in " << hitWins << " wins, " << runs - (hitWins + hitDraws)
       << " losses, and " << hitDraws << " draws." << endl;
   cout << "Probability stand resulted in " << standWins << " wins, " << runs - (standWins + standDraws)
       << " losses, and " << standDraws << " draws." << endl;
   float hitWinsFloat = hitWins;
   float standWinsFloat = standWins;
   cout << "Your probability of winning if you hit is " << (hitWinsFloat / runs) * 100 << "%" << endl;
cout << "Your probability of winning if you stand is " << (standWinsFloat / runs) * 100 << "%" << endl << endl;
}